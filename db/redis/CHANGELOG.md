### v0.1.1
1. fix config options
### v0.1.0
1. redis init
### 关于打版本的说明
```text
git tag    查询所有版本   
git tag -a 'db/redis/v0.1.0' -m 'v0.1.0'  创建本地的tag
git push  https://gitee.com/jokces/kit.git  'db/redis/v0.1.1'  推送tag到对应的远程仓库。
git tag -d 'db/redis/v0.1.0'  删除对应的本地tag
参考 :  mod 与 tag的关系
       https://www.jianshu.com/p/fe3457cfbb7a
       
       db/redis/v0.1.1  只会打对应db redis 下mod的文件
       
调用：  gitee.com/jokces/kit/cloud
       gitee.com/jokces/kit/db/redis
       gitee.com/jokces/kit/db/mysql
       gitee.com/jokces/kit/db/mongo
       
       
当出现子包不存在的情况下： 采用如下方式
       go mod edit -replace gitee.com/jokces/kit=gitee.com/jokces/kit@v0.10.0
       
       go mod tidy
       
       go get gitee.com/jokces/kit
       
       
       go get gitee.com/jokces/kit/db/redis@latest //获取最新版本
```
