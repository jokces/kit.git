package redis

import (
	"context"
	"fmt"
	"testing"
	"time"
)

var cli *Client
var ctx context.Context

func init() {
	ctx = context.Background()
	client, err := NewRedisClient(Config{
		Addr: "124.222.135.192:6379",
		DB:   10,
		Options: Options{
			Password: "OnlyOne2023*",
		},
	})
	if err != nil {
		panic(err)
		return
	}
	cli = client
}

type Student struct {
	Name string     `json:"name"`
	Age  int        `json:"age"`
	Arr  []*Student `json:"arr,omitempty"`
}

func (s *Student) Key() string {
	return fmt.Sprintf("stu:%v", s.Name)
}

func Test_Cache_Db(t *testing.T) {
	in := &Student{Name: "22222"}

	if err := cli.DelCache()(ctx, in, func(ctx context.Context, in interface{}) error {
		return nil
	}); err != nil {
		t.Error(err)
		return
	}

	rs, err := cli.WithCache(5*time.Minute, &Student{})(ctx, in, func(ctx context.Context, in interface{}) (interface{}, error) {
		out, err := queryUserByName(in.(*Student))
		if err != nil {
			return out, err
		}
		in = out
		return out, nil
	})

	if err != nil {
		panic(err)
		return
	}

	student := rs.(*Student)
	json, err := toJson(student)
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Printf("val=%v\n", json)
}

func Test_Cache(t *testing.T) {

	key := "1213131313"

	//if err := cli.DelKeyCache()(ctx, key, func() error {
	//	//TODO: 执行删除的操作
	//	return nil
	//}); err != nil {
	//	t.Error(err)
	//	return
	//}

	rs := make([]*Student, 0)
	_, err := cli.KeyWithCache(15*time.Minute, &rs)(ctx, key, func() (interface{}, error) {
		byName, err := queryByName("111")
		rs = byName
		return byName, err
	})
	if err != nil {
		t.Error(err)
		return
	}

	for _, v := range rs {
		fmt.Printf("val=%v\n", v)
	}

}

func Test_String_Cache(t *testing.T) {

	key := "555555124"

	//if err := cli.DelKeyCache()(ctx, key, func() error {
	//	//TODO: 执行删除的操作
	//	return nil
	//}); err != nil {
	//	t.Error(err)
	//	return
	//}

	rs := make([]*Student, 0)
	text, err := cli.KeyWithStringCache(15*time.Minute)(ctx, key, func() (interface{}, error) {
		byName, err := queryByName("111")
		return byName, err
	})
	if err != nil {
		t.Error(err)
		return
	}
	if err = toObject(text, &rs); err != nil {
		t.Error(err)
		return
	}
	for _, v := range rs {
		fmt.Printf("val=%v\n", v)
	}

}

func queryByName(name string) ([]*Student, error) {
	fmt.Printf("查询数据库 >>>>>>>> conditon=%v \n", name)
	ra := make([]*Student, 0)
	ra = append(ra, &Student{
		Name: name,
		Age:  10,
	})
	return ra, nil
}

func queryUserByName(in *Student) (*Student, error) {
	//完成业务逻辑
	fmt.Printf("查询数据库 >>>>>>>> conditon=%v \n", in)

	lk := make([]*Student, 0)
	lk = append(lk, &Student{
		Name: "xxxxxx",
		Age:  10,
	})

	return &Student{
		Name: "1111111",
		Age:  10,
		Arr:  lk,
	}, nil
}
