package enum

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

// DelStatus 删除状态
type DelStatus int32

const (
	Common DelStatus = iota //0- 正常
	DEL                     //1- 删除
)

// CommonStatus 状态信息,基础状态
type CommonStatus int32

const (
	INIT     CommonStatus = iota //0- 表示默认状态
	ABLE                         //1- 正常
	DISABLED                     //2- 停用，限制状态
)

// JsonUnixTime 时间序列化的处理
type JsonUnixTime int64

func (j JsonUnixTime) MarshalJSON() ([]byte, error) {
	if j > 0 {
		return []byte(`"` + time.Unix(int64(j), 0).Format("2006-01-02 15:04:05") + `"`), nil
	} else {
		return []byte(`""`), nil
	}
}
func (j *JsonUnixTime) UnmarshalJSON(b []byte) error {
	parse, err := time.Parse("2006-01-02 15:04:05", strings.Replace(string(b), `"`, "", -1))
	if err != nil {
		return err
	}
	*j = JsonUnixTime(parse.Unix())
	return nil
}
func (j JsonUnixTime) ToUnix() int64 {
	return int64(j)
}

type JsonTime time.Time

func (j JsonTime) MarshalJSON() ([]byte, error) {
	return []byte(`"` + time.Time(j).Format("2006-01-02 15:04:05") + `"`), nil
}
func (j *JsonTime) UnmarshalJSON(b []byte) error {
	parse, err := time.Parse("2006-01-02 15:04:05", strings.Replace(string(b), `"`, "", -1))
	if err != nil {
		return err
	}
	*j = JsonTime(parse)
	return nil
}
func (j JsonTime) ToUnix() int64 {
	return time.Time(j).Unix()
}

// JsonString json字符串
type JsonString string

func (s JsonString) String() string {
	return string(s)
}
func (s JsonString) ToModel(out interface{}) error {
	return json.Unmarshal([]byte(s.String()), out)
}

// MobileDes 脱敏的处理
type MobileDes string

func (d MobileDes) MarshalJSON() ([]byte, error) {
	buf := Mobile(string(d))
	return []byte(`"` + buf + `"`), nil
}

type EmailDes string

func (d EmailDes) MarshalJSON() ([]byte, error) {
	buf := Email(string(d))
	return []byte(`"` + buf + `"`), nil
}

type CardDes string

func (d CardDes) MarshalJSON() ([]byte, error) {
	buf := IdCard(string(d))
	return []byte(`"` + buf + `"`), nil
}

// ToJson 转换JSON
func ToJson(obj interface{}) string {
	b, _ := json.Marshal(obj)
	return string(b)
}
func ToObject(val string, obj interface{}) error {
	return json.Unmarshal([]byte(val), obj)
}

// Dic 字典的处理
var Dic = map[string]string{}

type DicEnum string

func (d DicEnum) MarshalJSON() ([]byte, error) {
	val := Dic[string(d)]
	return []byte(fmt.Sprintf(`{"key": "%s", "value":"%s"}`, d, val)), nil
}

// AreaVo 区域的处理
type AreaVo struct {
	Ids   string `json:"ids"`   //区域id，例如: 50010,52201,5220
	Value string `json:"value"` //区域值, 例如：重庆市,重庆市,渝中区
}

func (a *AreaVo) GetCode() string {
	return a.Ids
}
func (a *AreaVo) GetValue() string {
	return a.Value
}

type CommonIdReq struct {
	Id int64 `json:"id" form:"id"          binding:"required,min=1"` //ID
}
type CommonDisabledReq struct {
	Id     int64        `json:"id"           form:"id"            binding:"required,min=1"`       //id
	Status CommonStatus `json:"status"       form:"status"        binding:"required,gte=1,lte=2"` //状态; 1-启用，2-停用
}
type CommonStatusReq struct {
	Status CommonStatus `json:"status"       form:"status"        binding:"required,gte=1,lte=2"` //状态; 1-启用，2-停用
}
