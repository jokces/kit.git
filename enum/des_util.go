package enum

import "strings"

//脱敏工具

// Mobile 手机号脱敏
func Mobile(mobile string) string {
	if mobile == "" {
		return "***"
	}
	ml := len(mobile)
	if ml > 4 {
		return mobile[0:3] + "***" + mobile[ml-3:]
	}
	if ml > 0 {
		return mobile[0:1] + "***"
	}
	return "***"
}

func Email(email string) string {
	if email == "" {
		return "***"
	}
	split := strings.Split(email, "@")
	if len(split) < 2 {
		return email
	}
	prefix := split[0]

	var buf string
	ml := len(prefix)
	if ml > 4 {
		buf = prefix[0:3] + "***" + prefix[ml-3:]
	} else if ml > 0 {
		buf = prefix[0:1] + "***"
	} else {
		buf = "***"
	}
	return buf + "@" + split[1]
}

func IdCard(card string) string {
	if card == "" {
		return "******"
	}
	ml := len(card)
	if ml == 18 {
		return card[0:3] + "******" + card[ml-3:]
	}
	if ml > 0 {
		return card[0:1] + "******"
	}
	return "******"
}
