package uid

import (
	"gitee.com/jokces/kit/errc"
	"github.com/jinzhu/now"
	"github.com/xingliuhua/leaf"
	"strings"
)

func OrderNum(param ...string) (string, error) {
	//采用雪花算法获得
	err, node := leaf.NewNode(1)
	if err != nil {
		return "", errc.ErrFailed.MultiMsg("get order number failed")
	}
	err, id := node.NextId()
	if err != nil {
		return "", errc.ErrFailed.MultiMsg("get order number failed")
	}
	var build strings.Builder
	dateTime := now.BeginningOfDay().Format("20060102")
	build.WriteString(dateTime)
	build.WriteString(id)
	if param != nil {
		for _, str := range param {
			build.WriteString(str)
		}
	}
	return build.String(), nil
}
