package global

import (
	"fmt"
	"gitee.com/jokces/kit/global/glconf"
	"testing"
)

func Test_Global(t *testing.T) {

	c := &glconf.Config{}

	BootStrap(c)

	a := &A{
		Name: "xxx",
	}

	ma := make(map[string]string)
	s := ma[a.Str()]
	fmt.Printf(s)
}

type A struct {
	Name string
}

func (a *A) Str() string {
	return a.Name
}
