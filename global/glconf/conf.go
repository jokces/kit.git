package glconf

import (
	"gitee.com/jokces/kit/db/mysql"
	"gitee.com/jokces/kit/db/redis"
)

var Models = make([]interface{}, 0)

func AddSyncModels(m ...interface{}) {
	Models = append(Models, m...)
}

type Config struct {
	Env   string        `defval:"dev"`
	Lang  string        `defval:"CH"`
	Mysql XormConf      //数据库配置
	Redis RedisConf     //Redis配置
	Oss   FileConf      //文件配置
	Msg   MsgConf       //信息，短信，邮件配置
	Pay   ThirdPaysConf //支付配置
}

type XormConf struct {
	mysql.Config
	Enable bool `defval:"false"` //true：表示启用
}
type RedisConf struct {
	redis.Config
	Enable bool `defval:"false"` //true：表示启用
}

func (c *Config) Release() bool {
	return c.Env == "release"
}

// FileConf 文件上传的处理
type FileConf struct {
	OssConf
	LocalConf
}

// OssConf 阿里云的配置
type OssConf struct {
	Endpoint        string `json:"endpoint"` //Endpoint
	AccessKeyId     string `json:"accessKeyId"`
	AccessKeySecret string `json:"accessKeySecret"`
	Buket           string `json:"buket"`
	FilePath        string `json:"filePath"`
	ImageUrl        string `json:"imageUrl"`
	Enable          bool   `defval:"false"` //true：表示启用
}

// LocalConf 本地文件配置
type LocalConf struct {
	FilePath     string `defval:"/image"`
	DownloadPath string `defval:"/image/admin"`
	FileUrl      string `defval:""`
	Enable       bool   `defval:"false"` //true：表示启用
}

// MsgConf 信息化的处理
type MsgConf struct {
	EmailConf
	AliSmsConf
	MsgTemplate
}

// EmailConf 邮件配置
type EmailConf struct {
	User   string `json:"user"`    //发送方邮件
	Pass   string `json:"pass"`    //账户密码
	Host   string `json:"host"`    //发送的协议
	Enable bool   `defval:"false"` //true：表示启用
}

// AliSmsConf 阿里云短信
type AliSmsConf struct {
	RegionId        string `json:"regionId"`        //请求的路径
	AccessKey       string `json:"accessKey"`       //key
	AccessKeySecret string `json:"accessKeySecret"` //秘钥
	SignName        string `json:"signName"`        //签名
	TempChId        string `json:"tempChId"`        //模版ID
	TempEnId        string `json:"tempEnId"`        //模版ID
	Enable          bool   `defval:"false"`         //true：表示启用
}

// MsgTemplate 消息模版的配置
type MsgTemplate struct {
	Temp   map[string]*TemplateConf
	MaxDay int64 `defval:"5"`     //每天获取的短信最大次数
	IsMock bool  `defval:"false"` //是否模拟，如果不验证则直接初始1234
}
type TemplateConf struct {
	Subject string `json:"subject"` //主题模式
	Txt     string `json:"txt"`     //模版内容
}

// FindTemp 返回主题，模版
func (mc *MsgConf) FindTemp(code string) (string, string) {
	if mc.MsgTemplate.Temp == nil {
		return "hello", "%v"
	}
	conf := mc.MsgTemplate.Temp[code]
	if conf == nil {
		return "hello", "%v"
	}
	return conf.Subject, conf.Txt
}

// 支付配置
type ThirdPaysConf struct {
	ExpTime int64       `json:"expTime"` //支付有效时间s
	Enable  bool        `json:"enable"`  //是否开启;true:表示开启的
	Al      *AliPayConf `json:"al"`      //支付宝配置
	Wx      *WxPayConf  `json:"wx"`      //微信支付
}

type AliPayConf struct {
	//privateKey := "MIIEogIBAAKCAQEAy+CRzKw4krA2RzCDTqg5KJg92XkOY0RN3pW4sYInPqnGtHV7YDHu5nMuxY6un+dLfo91OFOEg+RI+WTOPoM4xJtsOaJwQ1lpjycoeLq1OyetGW5Q8wO+iLWJASaMQM/t/aXR/JHaguycJyqlHSlxANvKKs/tOHx9AhW3LqumaCwz71CDF/+70scYuZG/7wxSjmrbRBswxd1Sz9KHdcdjqT8pmieyPqnM24EKBexHDmQ0ySXvLJJy6eu1dJsPIz+ivX6HEfDXmSmJ71AZVqZyCI1MhK813R5E7XCv5NOtskTe3y8uiIhgGpZSdB77DOyPLcmVayzFVLAQ3AOBDmsY6wIDAQABAoIBAHjsNq31zAw9FcR9orQJlPVd7vlJEt6Pybvmg8hNESfanO+16rpwg2kOEkS8zxgqoJ1tSzJgXu23fgzl3Go5fHcoVDWPAhUAOFre9+M7onh2nPXDd6Hbq6v8OEmFapSaf2b9biHnBHq5Chk08v/r74l501w3PVVOiPqulJrK1oVb+0/YmCvVFpGatBcNaefKUEcA+vekWPL7Yl46k6XeUvRfTwomCD6jpYLUhsAKqZiQJhMGoaLglZvkokQMF/4G78K7FbbVLMM1+JDh8zJ/DDVdY2vHREUcCGhl4mCVQtkzIbpxG++vFg7/g/fDI+PquG22hFILTDdtt2g2fV/4wmkCgYEA6goRQYSiM03y8Tt/M4u1Mm7OWYCksqAsU7rzQllHekIN3WjD41Xrjv6uklsX3sTG1syo7Jr9PGE1xQgjDEIyO8h/3lDQyLyycYnyUPGNNMX8ZjmGwcM51DQ/QfIrY/CXjnnW+MVpmNclAva3L33KXCWjw20VsROV1EA8LCL94BUCgYEA3wH4ANpzo7NqXf+2WlPPMuyRrF0QPIRGlFBNtaKFy0mvoclkREPmK7+N4NIGtMf5JNODS5HkFRgmU4YNdupA2I8lIYpD+TsIobZxGUKUkYzRZYZ1m1ttL69YYvCVz9Xosw/VoQ+RrW0scS5yUKqFMIUOV2R/Imi//c5TdKx6VP8CgYAnJ1ADugC4vI2sNdvt7618pnT3HEJxb8J6r4gKzYzbszlGlURQQAuMfKcP7RVtO1ZYkRyhmLxM4aZxNA9I+boVrlFWDAchzg+8VuunBwIslgLHx0/4EoUWLzd1/OGtco6oU1HXhI9J9pRGjqfO1iiIifN/ujwqx7AFNknayG/YkQKBgD6yNgA/ak12rovYzXKdp14Axn+39k2dPp6J6R8MnyLlB3yruwW6NSbNhtzTD1GZ+wCQepQvYvlPPc8zm+t3tl1r+Rtx3ORf5XBZc3iPkGdPOLubTssrrAnA+U9vph61W+OjqwLJ9sHUNK9pSHhHSIS4k6ycM2YAHyIC9NGTgB0PAoGAJjwd1DgMaQldtWnuXjvohPOo8cQudxXYcs6zVRbx6vtjKe2v7e+eK1SSVrR5qFV9AqxDfGwq8THenRa0LC3vNNplqostuehLhkWCKE7Y75vXMR7N6KU1kdoVWgN4BhXSwuRxmHMQfSY7q3HG3rDGz7mzXo1FVMr/uE4iDGm0IXY="
	PrivateKey string `json:"privateKey"` //应用私钥，支持PKCS1和PKCS8
	PublicKey  string `json:"publicKey"`  //公钥
	AppId      string `json:"appId"`      //应用ID "2016091200494382"
	IsProd     bool   `json:"isProd"`     //是否是正式环境
	Notify     string `json:"notify"`     //回调的url
	Enable     bool   `json:"enable"`     //是否开启
}

type WxPayConf struct {
	PrivateKey string `json:"privateKey"` //privateKey：商户API证书下载后，私钥 apiclient_key.pem 读取后的字符串内容
	ApiV3Key   string `json:"apiV3Key"`   //apiV3Key：APIv3Key，商户平台获取
	MchId      string `json:"mchId"`      //商户id
	AppId      string `json:"appId"`      //公众号ID
	SerialNo   string `json:"serialNo"`   //商户API证书的证书序列号
	Enable     bool   `json:"enable"`     //是否开启
	Notify     string `json:"notify"`     //回调的url
}
