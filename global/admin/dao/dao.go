package sysDao

import (
	"gitee.com/jokces/kit/db/mysql"
	"gitee.com/jokces/kit/global"
)

type Dao struct {
	dao *mysql.Xorm
}

func NewDao() *Dao {
	return &Dao{
		dao: global.GetDb(),
	}
}

func (d *Dao) GetDao() *mysql.Xorm {
	return d.dao
}
