package sysDao

import (
	"context"
	"gitee.com/jokces/kit/errc"
	sysModel "gitee.com/jokces/kit/global/admin/model"
	"gitee.com/jokces/kit/typ"
)

func (d *Dao) ClientList(ctx context.Context, req *sysModel.SysOauthClient, page *typ.PageReq) (int64, []*sysModel.SysOauthClient, error) {
	arr := make([]*sysModel.SysOauthClient, 0)
	sess := d.dao.Context(ctx)
	sess.And("is_del = 0")
	if req.Id > 0 {
		sess.And("id = ?", req.Id)
	}
	if req.ClientId != "" {
		sess.And("client_id = ?", req.ClientId)
	}
	if req.ClientName != "" {
		sess.And("client_name = ?", req.ClientName)
	}
	if page != nil {
		count, err := sess.Desc("id").Limit(page.LimitStart()).FindAndCount(&arr)
		return count, arr, errc.WithStack(err)
	}
	err := sess.Desc("id").Find(&arr)
	return int64(len(arr)), arr, errc.WithStack(err)
}

func (d *Dao) ClientOne(ctx context.Context, req *sysModel.SysOauthClient) (bool, *sysModel.SysOauthClient, error) {
	if req.QueryOneVal() {
		return false, req, errc.ErrParamInvalid.MultiMsg("id or clientId need")
	}
	rs := &sysModel.SysOauthClient{}

	sess := d.dao.Context(ctx)
	sess.And("is_del = 0")
	if req.Id > 0 {
		sess.And("id = ?", req.Id)
	}
	if req.ClientId != "" {
		sess.And("client_id = ?", req.ClientId)
	}
	exist, err := sess.Get(rs)
	return exist, rs, errc.WithStack(err)
}
