package sysDao

import (
	"context"
	"gitee.com/jokces/kit/errc"
	sysModel "gitee.com/jokces/kit/global/admin/model"
)

func (d *Dao) AccountOne(ctx context.Context, req *sysModel.AccountSession) (bool, *sysModel.AccountSession, error) {
	rs := &sysModel.AccountSession{}
	if ok := req.OneParamCheck(); ok {
		return false, nil, errc.ErrParamInvalid
	}
	sess := d.dao.Context(ctx).Table(sysModel.AccountTable).Alias("a")
	if req.Id > 0 {
		sess.And("a.id = ?", req.Id)
	}
	if req.Uid != "" {
		sess.And("a.uid = ?", req.Uid)
	}
	if req.InviteId > 0 {
		sess.And("a.invite_id = ?", req.InviteId)
	}
	if req.InviteCode != "" {
		sess.And("a.invite_code = ?", req.InviteCode)
	}
	if req.Mobile != "" {
		sess.And("a.mobile = ?", req.Mobile)
	}
	if req.Email != "" {
		sess.And("a.email = ?", req.Email)
	}
	exist, err := sess.Select("a.id,a.uid,a.invite_id,a.invite_code,a.mobile,a.email,a.user_status,a.pass,a.salt").Get(rs)
	return exist, rs, errc.WithStack(err)
}
func (d *Dao) AccountThirdOne(ctx context.Context, req *sysModel.AccountThird) (bool, *sysModel.AccountThird, error) {
	rs := &sysModel.AccountThird{}
	sess := d.dao.Context(ctx).Table(sysModel.AccountThirdTable).Alias("a")
	if req.Id > 0 {
		sess.And("a.id = ?", req.Id)
	}
	if req.AppId != "" {
		sess.And("a.app_id = ?", req.AppId)
	}
	if req.UserId > 0 {
		sess.And("a.user_id = ?", req.UserId)
	}
	if req.UnionId != "" {
		sess.And("a.union_id = ?", req.UnionId)
	}
	if req.OpenId != "" {
		sess.And("a.open_id = ?", req.OpenId)
	}
	exist, err := sess.Select("a.id,a.app_id,a.user_id,a.union_id,a.open_id").Get(rs)
	return exist, rs, errc.WithStack(err)
}
