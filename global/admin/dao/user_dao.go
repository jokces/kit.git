package sysDao

import (
	"context"
	"gitee.com/jokces/kit/errc"
	sysModel "gitee.com/jokces/kit/global/admin/model"
	"gitee.com/jokces/kit/typ"
)

func (d *Dao) UserPageList(ctx context.Context, req *sysModel.SysUser, pageReq *typ.PageReq) (int64, []*sysModel.SysUser, error) {
	list := make([]*sysModel.SysUser, 0)
	session := d.dao.Context(ctx)

	if req.Id > 0 {
		session.And("id = ?", req.Id)
	}
	if req.DepId > 0 {
		session.And("dep_id = ?", req.DepId)
	}
	if req.RoleId > 0 {
		session.And("role_id = ?", req.RoleId)
	}
	if req.Status > 0 {
		session.And("`status` = ?", req.Status)
	}
	if req.Uid != "" {
		session.And("uid = ?", req.Uid)
	}
	if req.Email != "" {
		session.And("email = ?", req.Email)
	}
	if req.Phone != "" {
		session.And("phone = ?", req.Phone)
	}
	if req.UserName != "" {
		session.And("user_name = ?", req.UserName)
	}
	if req.LikeUserName != "" {
		session.And("user_name like ?", req.LikeUserName+"%")
	}
	session.Desc("id")
	if pageReq != nil {
		count, err := session.Limit(pageReq.LimitStart()).FindAndCount(&list)
		return count, list, errc.WithStack(err)
	} else {
		err := session.Find(&list)
		return int64(len(list)), list, errc.WithStack(err)
	}
}
func (d *Dao) UserGetOne(ctx context.Context, in sysModel.SysUser) (bool, *sysModel.SysUser, error) {
	if ok := in.CheckOneParam(); ok {
		return false, nil, errc.ErrNotFound.MultiMsg("参数不正确!")
	}
	row, users, err := d.UserPageList(ctx, &in, nil)
	if err != nil {
		return false, &sysModel.SysUser{}, errc.WithStack(err)
	}
	if row == 0 {
		return false, &sysModel.SysUser{}, nil
	}
	return true, users[0], nil
}

// CountMobileOrEmail 检查手机号获取邮箱是否存在
func (d *Dao) CountMobileOrEmail(phone, email string) (bool, error) {
	sql := `
		SELECT sum(t.c) from(
		SELECT count(1) as c from sys_user where phone = ?
		UNION ALL
		SELECT count(1) as c from sys_user where email = ?
		) as t
    `
	if phone == "" {
		phone = "0"
	}
	if email == "" {
		email = "0"
	}
	type count struct {
		Num int
	}
	row := &count{}
	exist, err := d.dao.SQL(sql, phone, email).Get(row)
	if err != nil {
		return false, err
	}
	return exist && row.Num > 0, nil
}

// AdminSessionGet 获取对应的session用户
func (d *Dao) AdminSessionGet(ctx context.Context, userId int64) (bool, *sysModel.AdminUserToken, error) {
	rs := &sysModel.AdminUserToken{}
	exist, err := d.dao.Context(ctx).
		Table("sys_user").Alias("u").
		Join("INNER", []string{"sys_role", "s"}, "u.role_id = s.id").
		Join("INNER", []string{"sys_dep", "d"}, "u.dep_id = d.id").
		Where("u.id = ? and u.`status` = 1 and d.`status` = 1 and s.`status` = 1", userId).
		Select("u.uid,u.id,u.nick_name,u.user_name,u.phone,u.email,u.role_id,s.typ as role_typ,s.is_root as root_typ,s.role_name,u.dep_id,d.dep_code,d.dep_name").Get(rs)
	return exist, rs, errc.WithStack(err)
}
func (d *Dao) AdminRoleGet(ctx context.Context, userId int64) (bool, *sysModel.AdminUserToken, error) {
	rs := &sysModel.AdminUserToken{}
	exist, err := d.dao.Context(ctx).
		Table("sys_user").Alias("u").
		Join("INNER", []string{"sys_role", "s"}, "u.role_id = s.id").
		Where("u.id = ?", userId).
		Select("u.uid,u.id,u.user_name,u.phone,u.email,u.role_id,s.typ as role_typ,s.is_root as root_typ").Get(rs)
	return exist, rs, errc.WithStack(err)
}
