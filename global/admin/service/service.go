package sysService

import (
	"gitee.com/jokces/kit/db/redis"
	"gitee.com/jokces/kit/global"
	sysDao "gitee.com/jokces/kit/global/admin/dao"
	"gitee.com/jokces/kit/global/file"
	"gitee.com/jokces/kit/global/glconf"
	"gitee.com/jokces/kit/global/sms"
	"sync"
)

var SysService *Service
var lock = sync.Mutex{}

type Service struct {
	d *sysDao.Dao
	f *file.File
	s *sms.MegHandle
	r *redis.Client
	c *glconf.Config
}

func NewService() *Service {
	if SysService != nil {
		return SysService
	}
	lock.Lock()
	defer lock.Unlock()
	SysService = &Service{
		d: sysDao.NewDao(),
		c: global.GetCfg(),
		f: global.GetFile(),
		r: global.GetRedis(),
		s: global.GetMsg(),
	}
	return SysService
}
