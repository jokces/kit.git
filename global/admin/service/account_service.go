package sysService

import (
	"context"
	"gitee.com/jokces/kit/enum"
	"gitee.com/jokces/kit/errc"
	sysModel "gitee.com/jokces/kit/global/admin/model"
	"gitee.com/jokces/kit/global/admin/transet"
)

func (svc *Service) AccountOne(ctx context.Context, in *sysModel.AccountSession) (*sysModel.AccountSession, error) {
	exist, user, err := svc.d.AccountOne(ctx, in)
	if err != nil {
		return nil, err
	}
	if !exist {
		return nil, errc.ErrNotFound.WithEnum(transet.UserNotFund)
	}
	if user.UserStatus == enum.DISABLED {
		return nil, errc.ErrNotFound.WithEnum(transet.UserDisabled)
	}
	return user, nil
}
func (svc *Service) AccountPass(ctx context.Context, in *sysModel.AccountSession) (*sysModel.AccountSession, error) {
	user, err := svc.AccountOne(ctx, in)
	if err != nil {
		return nil, err
	}
	if checkPass := user.CheckPass(in.Pass); !checkPass {
		return nil, errc.ErrNotFound.WithEnum(transet.UserNotMatch)
	}
	return user, nil
}

func (svc *Service) AccountThirdOne(ctx context.Context, in *sysModel.AccountThird) (*sysModel.AccountThird, error) {
	exist, user, err := svc.d.AccountThirdOne(ctx, in)
	if err != nil {
		return nil, err
	}
	if !exist {
		return nil, errc.ErrNotFound.WithEnum(transet.UserNotFund)
	}
	return user, nil
}
