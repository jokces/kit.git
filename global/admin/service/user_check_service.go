package sysService

import (
	"context"
	"gitee.com/jokces/kit/enum"
	"gitee.com/jokces/kit/errc"
	"gitee.com/jokces/kit/gen/str"
	sysModel "gitee.com/jokces/kit/global/admin/model"
)

func (svc *Service) userAddCheck(ctx context.Context, req *sysModel.UserAddReq) (*sysModel.SysUser, error) {

	if exist, _, err := svc.d.UserGetOne(ctx, sysModel.SysUser{
		UserName: req.UserName,
	}); err != nil {
		return nil, errc.WithStack(err)
	} else if exist {
		return nil, errc.ErrParamInvalid.MultiMsg("账号名已存在")
	}

	if req.Email != "" {
		if exist, _, err := svc.d.UserGetOne(ctx, sysModel.SysUser{
			Email: req.Email,
		}); err != nil {
			return nil, errc.WithStack(err)
		} else if exist {
			return nil, errc.ErrParamInvalid.MultiMsg("邮箱已被注册")
		}
	}

	if req.Phone != "" {
		if exist, _, err := svc.d.UserGetOne(ctx, sysModel.SysUser{
			Phone: req.Phone,
		}); err != nil {
			return nil, errc.WithStack(err)
		} else if exist {
			return nil, errc.ErrParamInvalid.MultiMsg("手机号已被注册")
		}
	}

	if exist, _, err := svc.d.UserGetOne(ctx, sysModel.SysUser{
		UserName: req.UserName,
	}); err != nil {
		return nil, errc.WithStack(err)
	} else if exist {
		return nil, errc.ErrParamInvalid.MultiMsg("账号名注册")
	}

	if exist, _, err := svc.d.DepGetOne(ctx, &sysModel.SysDep{
		Id: req.DepId,
	}); err != nil {
		return nil, errc.WithStack(err)
	} else if !exist {
		return nil, errc.ErrParamInvalid.MultiMsg("部门不存在")
	}

	if exist, _, err := svc.d.RoleGetOne(ctx, &sysModel.SysRole{
		Id: req.RoleId,
	}); err != nil {
		return nil, errc.WithStack(err)
	} else if !exist {
		return nil, errc.ErrParamInvalid.MultiMsg("角色不存在")
	}
	u := &sysModel.SysUser{
		UserName: req.UserName,
		NickName: req.Nickname,
		Phone:    req.Phone,
		Email:    req.Email,
		Salt:     str.RandNumCode(6),
		DepId:    req.DepId,
		RoleId:   req.RoleId,
		Status:   enum.ABLE,
	}
	return u, nil
}
func (svc *Service) userEditCheck(ctx context.Context, req *sysModel.UserEditReq) (*sysModel.SysUser, error) {
	exist, user, err := svc.d.UserGetOne(ctx, sysModel.SysUser{
		Id: req.Id,
	})
	if err != nil {
		return nil, errc.WithStack(err)
	} else if !exist {
		return nil, errc.ErrNotFound.MultiMsg("用户不存在")
	}

	u := &sysModel.SysUser{
		NickName: req.Nickname,
	}

	//手机号
	if req.Phone != "" && req.Phone != user.Phone {
		if exist, _, err := svc.d.UserGetOne(ctx, sysModel.SysUser{
			Phone: req.Phone,
		}); err != nil {
			return nil, errc.WithStack(err)
		} else if exist {
			return nil, errc.ErrParamInvalid.MultiMsg("手机号已被注册")
		}
		u.Phone = req.Phone
	}

	//邮箱
	if req.Email != "" && req.Email != user.Email {
		if exist, _, err := svc.d.UserGetOne(ctx, sysModel.SysUser{
			Email: req.Email,
		}); err != nil {
			return nil, errc.WithStack(err)
		} else if exist {
			return nil, errc.ErrParamInvalid.MultiMsg("邮箱地址已被注册")
		}
		u.Email = req.Email
	}

	if req.RoleId != user.RoleId {
		exist, role, err := svc.d.RoleGetOne(ctx, &sysModel.SysRole{
			Id: req.RoleId,
		})
		if err != nil {
			return nil, errc.WithStack(err)
		} else if !exist {
			return nil, errc.ErrParamInvalid.MultiMsg("角色不存在")
		} else if role.Status != enum.ABLE {
			return nil, errc.ErrParamInvalid.MultiMsg("角色已禁用")
		}
		u.RoleId = req.RoleId
	}
	return u, nil
}

// 用户的菜单
func (svc *Service) userInfoMenu(ctx context.Context, sess sysModel.AdminUserToken) ([]*sysModel.UserMenus, error) {
	var total []*sysModel.SysMenu
	if sess.RootRole() {
		menus, err := svc.d.MenuList(ctx, &sysModel.SysMenu{
			Status:  enum.ABLE,
			GroupId: enum.DicEnum(sess.GroupId),
		})
		if err != nil {
			return nil, err
		}
		total = menus
	} else {
		roleMenus, err := svc.d.MenuListByRoleId(ctx, sess.RoleId)
		if err != nil {
			return nil, err
		}
		total = roleMenus
	}

	//查询菜单
	mlist := make([]*sysModel.UserMenus, 0) //菜单列表
	for _, menu := range total {
		if menu.MenuType != "B" {
			mlist = append(mlist, &sysModel.UserMenus{
				Id:        menu.Id,
				PId:       menu.ParentId,
				MenuName:  menu.MenuName,
				Title:     menu.Title,
				Icon:      menu.Icon,
				Sequence:  menu.Sequence,
				MenuType:  menu.MenuType,
				Show:      menu.Show,
				Component: menu.Component,
			})
		}
	}
	return mlist, nil
}

// 用户的角色
func (svc *Service) userRole(ctx context.Context, sess sysModel.AdminUserToken, out *sysModel.UserInfoResp) error {
	exist, role, err := svc.d.RoleGetOne(ctx, &sysModel.SysRole{
		Id: sess.RoleId,
	})
	if err != nil {
		return err
	}
	if !exist || role.Status == enum.DISABLED {
		return errc.ErrAccessDenied.MultiMsg("无权限")
	}
	out.Roles = []string{role.RoleCode}

	//用户登陆后的菜单列表
	var total []*sysModel.SysMenu
	if sess.RootRole() {
		menus, err := svc.d.MenuList(ctx, &sysModel.SysMenu{
			Status:  enum.ABLE,
			GroupId: enum.DicEnum(sess.GroupId),
		})
		if err != nil {
			return err
		}
		total = menus
	} else {
		roleMenus, err := svc.d.MenuListByRoleId(ctx, sess.RoleId)
		if err != nil {
			return err
		}
		total = roleMenus
	}

	mlist := make([]*sysModel.UserMenus, 0) //菜单列表
	clist := make([]string, 0)
	for _, menu := range total {
		if menu.MenuType == "B" {
			clist = append(clist, menu.Permission)
		} else {
			mlist = append(mlist, &sysModel.UserMenus{
				Id:        menu.Id,
				PId:       menu.ParentId,
				MenuName:  menu.MenuName,
				Title:     menu.Title,
				Icon:      menu.Icon,
				Sequence:  menu.Sequence,
				MenuType:  menu.MenuType,
				Show:      menu.Show,
				Component: menu.Component,
			})
		}
	}
	out.Permissions = clist
	out.Menus = mlist
	return nil
}
