package sysService

import (
	"context"
	"fmt"
	"gitee.com/jokces/kit/enum"
	"gitee.com/jokces/kit/errc"
	"gitee.com/jokces/kit/global"
	sysModel "gitee.com/jokces/kit/global/admin/model"
)

// DepTreeList 部门
func (svc *Service) DepTreeList(ctx context.Context, in *sysModel.DepTreeReq, sess sysModel.AdminUserToken) ([]*sysModel.SysDep, error) {
	query := sess.DepGet()
	query.Status = in.Status
	query.LikeDepName = in.DepName

	deps, err := svc.d.DepList(ctx, query)
	if err != nil {
		return nil, errc.WithStack(err)
	}
	//处理菜单的排序状态
	if len(deps) > 0 {
		recursive := depTreeRecursive(deps, deps[0].ParentId)
		return recursive, nil
	}
	return nil, nil
}
func depTreeRecursive(list []*sysModel.SysDep, parentId int64) []*sysModel.SysDep {
	res := make([]*sysModel.SysDep, 0)
	for _, v := range list {
		if v.ParentId == parentId {
			v.Children = depTreeRecursive(list, v.Id)
			res = append(res, v)
		}
	}
	return res
}
func (svc *Service) DepAdd(ctx context.Context, req *sysModel.DepAddReq) error {
	sa := &sysModel.SysDep{
		ParentId: req.ParentId,
		DepName:  req.DepName,
		Status:   req.Status,
	}

	if exist, _, err := svc.d.DepGetOne(ctx, &sysModel.SysDep{
		DepName: req.DepName,
	}); err != nil {
		return err
	} else if exist {
		return errc.ErrParamInvalid.MultiMsg("部门名称已存在")
	}

	exist, pDep, err := svc.d.DepGetOne(ctx, &sysModel.SysDep{
		Id: req.ParentId,
	})
	if err != nil {
		return err
	}
	if !exist {
		return errc.ErrParamInvalid.MultiMsg("上级部门不存在")
	}
	if pDep.Status != enum.ABLE {
		return errc.ErrParamInvalid.MultiMsg("上级部门不可用")
	}
	return svc.d.DepAdd(sa, pDep)
}
func (svc *Service) DepEdit(ctx context.Context, req *sysModel.DepEditReq) error {
	if req.Status < 1 || req.Status > 2 {
		return errc.ErrParamInvalid.MultiMsg("状态只能1|2")
	}
	if req.Id == 1 {
		return errc.ErrParamInvalid.MultiMsg("顶级部门不能编辑")
	}

	exist, dep, err := svc.d.DepGetOne(ctx, &sysModel.SysDep{
		Id: req.Id,
	})
	if err != nil {
		return err
	} else if !exist {
		return errc.ErrParamInvalid.MultiMsg("部门未找到")
	}

	if req.Status != enum.ABLE {
		//停用，需要检查下级
		if children, err := svc.d.DepList(ctx, &sysModel.SysDep{
			ParentId: dep.Id,
		}); err != nil {
			return err
		} else if len(children) > 0 {
			return errc.ErrParamInvalid.MultiMsg("请先停用子部门")
		}
	}

	return global.GetDb().TxUpdate(nil, dep.Id, &sysModel.SysDep{
		DepName: req.DepName,
		Status:  req.Status,
	}, []string{"dep_name", "`status`"})
}
func (svc *Service) DepDel(ctx context.Context, id int64) error {
	if id == 1 {
		return errc.ErrParamInvalid.MultiMsg("顶级部门不能删除")
	}

	exist, dep, err := svc.d.DepGetOne(ctx, &sysModel.SysDep{
		Id: id,
	})
	if err != nil {
		return err
	} else if !exist {
		return errc.ErrParamInvalid.MultiMsg("部门不存在")
	}

	deps, err := svc.d.DepList(ctx, &sysModel.SysDep{
		ParentId: id,
	})
	if err != nil {
		return err
	} else if len(deps) > 0 {
		return errc.ErrParamInvalid.MultiMsg("请先删除子部门")
	}

	return svc.d.GetDao().TxUpdate(nil, dep.Id, &sysModel.SysDep{
		DepName: fmt.Sprintf("%v_%v", dep.DepName, dep.Id),
		Status:  enum.DISABLED,
		IsDel:   int32(enum.DEL),
	}, []string{"dep_name", "`status`", "is_del"})
}
