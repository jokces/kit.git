package sysService

import (
	"gitee.com/jokces/kit/errc"
	"gitee.com/jokces/kit/global"
	sysModel "gitee.com/jokces/kit/global/admin/model"
	"strings"
	"time"
)

// CaptchaRedisCode 添加验证码到Redis中
func (svc *Service) CaptchaRedisCode(code, codeId string) error {
	err := global.GetRedis().SetWithTTL(codeId, code, sysModel.CodeExpired*time.Second)
	if err != nil {
		return err
	}
	return nil
}
func (svc *Service) CaptchaRedisVerify(code, codeId string) error {
	//验证验证码
	codeCache, err := svc.r.Get(codeId)
	if err != nil {
		return errc.ErrParamInvalid.MultiMsg("验证码不正确")
	}
	if codeCache == nil {
		return errc.ErrParamInvalid.MultiMsg("验证码已过期")
	}
	if strings.ToLower(codeCache.(string)) != strings.ToLower(code) {
		return errc.ErrParamInvalid.MultiMsg("验证码不正确")
	}
	return nil
}
