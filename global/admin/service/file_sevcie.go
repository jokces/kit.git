package sysService

import (
	"gitee.com/jokces/kit/errc"
	sysModel "gitee.com/jokces/kit/global/admin/model"
	"github.com/gin-gonic/gin"
	"time"
)

func (svc *Service) FileToUpload(c *gin.Context) (sysModel.FileModelResp, error) {
	rs := sysModel.FileModelResp{}
	local, err := svc.f.LocalUpload(c, "")
	if err != nil {
		return rs, err
	}
	if len(local.Url) <= 0 {
		return rs, errc.ErrParamInvalid.MultiMsg("上传文件失败")
	}
	rs.Url = local.Url
	rs.Timestamp = time.Now().Unix()
	return rs, nil
}

func (svc *Service) FileToUploadToOss(c *gin.Context) (sysModel.FileModelResp, error) {
	rs := sysModel.FileModelResp{}

	file, err := c.FormFile("file")
	if err != nil {
		return rs, errc.ErrParamInvalid.MultiMsg("请选择文件")
	}

	upload, err := svc.f.OssUpload(file)
	if err != nil {
		return rs, errc.ErrAuthInternalErr.MultiMsg("上传文件失败")
	}
	rs.Url = upload
	rs.Timestamp = time.Now().Unix()
	return rs, nil
}
