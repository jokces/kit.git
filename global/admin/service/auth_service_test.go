package sysService

import (
	"context"
	"fmt"
	"gitee.com/jokces/kit/enum"
	"gitee.com/jokces/kit/global"
	"gitee.com/jokces/kit/global/admin/conf"
	sysModel "gitee.com/jokces/kit/global/admin/model"
	"gitee.com/jokces/kit/global/admin/token"
	"gitee.com/jokces/kit/global/glconf"
	"gitee.com/jokces/kit/tests"
	"testing"
	"time"
)

func Test_JWT(t *testing.T) {
	//claims := jwt.NewCustomClaims("123", 15*time.Minute, sysModel.JWT)
	//
	//for i := 0; i < 5; i++ {
	//	token, _ := jwt.GenerateJWTToken(claims)
	//	fmt.Printf("%v\n", token)
	//	time.Sleep(30 * time.Second)
	//}

	ts := &token.Token{
		Id:  12345678910111213,
		Exp: time.Now().Unix() + 15*60*60,
		Typ: 1,
	}
	token.SetBase("1234567812345678123456781234567812345678123456781234567812345678")
	tokenStr, err := token.GenerateToken(ts)
	if err != nil {
		panic(err)
		return
	}
	fmt.Printf("token=%v\n", tokenStr)

	username, expired, err := token.ParseToken(tokenStr)
	if err != nil {
		panic(err)
		return
	}

	fmt.Printf("username=%v,expired=%v\n", username, expired)

}

var srv *Service
var cfg *glconf.Config
var ctx context.Context

func init() {
	config := conf.InitConfig()
	cfg = config
	global.BootStrap(config)

	srv = NewService()
	ctx = context.Background()
}

func Test_Client_Add(t *testing.T) {
	in := &sysModel.OauthClientAddReq{
		ClientSecret:         "123456789",
		Scope:                "server",
		GrantTypes:           "password,refresh_token,authorization_code,client_credentials,mobile",
		RedirectUri:          "",
		AccessTokenValidity:  3600,
		RefreshTokenValidity: 360000,
		AdditionalInfo:       "",
		ClientName:           "平台端",
		ClientId:             "000000",
	}
	if err := srv.AddClient(ctx, in); err != nil {
		t.Errorf("%v \n", err)
	}
}

func Test_ClientResource(t *testing.T) {
	in := &sysModel.ClientAddResourceReq{
		CommonIdReq: sysModel.CommonIdReq{
			Id: 1,
		},
		ResourceIds: "Menu_PlateForm",
	}
	if err := srv.ClientResource(ctx, in); err != nil {
		t.Errorf("%v \n", err)
	}
}

func Test_RedisClientById(t *testing.T) {
	//if rs, err := srv.ClientById(ctx, "000000"); err != nil {
	//	t.Errorf("%v \n", err)
	//} else {
	//	tests.PrintBeautifyJSON(rs)
	//}

	//if err := srv.RedisUpDateClient(ctx, &sysModel.OauthClientUpdateReq{
	//	CommonIdReq: sysModel.CommonIdReq{
	//		Id: 1,
	//	},
	//	OauthClientAddReq: sysModel.OauthClientAddReq{
	//		ClientId: "000000",
	//	},
	//}); err != nil {
	//	t.Errorf("%v \n", err)
	//	return
	//}

	if rs, err := srv.RedisClientById(ctx, "000000"); err != nil {
		t.Errorf("%v \n", err)
	} else {
		tests.PrintBeautifyJSON(rs)
	}
}

func Test_MenuAdd(t *testing.T) {
	if err := srv.MenuAdd(ctx, &sysModel.MenuAddReq{
		ParentId:   42,
		GroupId:    "MENU_RES_GROUP",
		MenuName:   "查询列表",
		Title:      "查询列表",
		Icon:       "",
		Sequence:   0,
		Status:     enum.ABLE,
		MenuType:   sysModel.Btn,
		Component:  "",
		Show:       1,
		Permission: "sys:client:list",
	}); err != nil {
		t.Errorf("%v \n", err)
	}
}

func Test_MenuEdit(t *testing.T) {
	if err := srv.MenuEdit(ctx, &sysModel.MenuEditReq{
		Id: 2,
		MenuAddReq: sysModel.MenuAddReq{
			ParentId:   1,
			GroupId:    "MENU_RES_GROUP",
			MenuName:   "部门管理",
			Title:      "部门管理",
			Icon:       "",
			Sequence:   0,
			Status:     enum.ABLE,
			MenuType:   sysModel.Menu,
			Component:  "",
			Show:       1,
			Permission: "sys:dep",
		},
	}); err != nil {
		t.Errorf("%v \n", err)
	}
}

func Test_ActionSaveOrUpdate(t *testing.T) {
	if err := srv.ActionSaveOrUpdate(ctx, &sysModel.ActionEditReq{
		GroupId: "MENU_RES_GROUP",
		ApiName: "【部门】列表树",
		Api:     "/admin/v1/dep/treeList",
		Status:  enum.ABLE,
	}); err != nil {
		t.Errorf("%v \n", err)
	}
}

// 查询用户角色的资源树列表
func Test_RolePermissionList(t *testing.T) {
	if rs, err := srv.RolePermissionList(ctx, 2); err != nil {
		t.Errorf("%v \n", err)
	} else {
		tests.PrintBeautifyJSON(rs)
	}
}

// 登陆后的用户详情
func Test_UserInfo(t *testing.T) {
	if rs, err := srv.UserInfo(ctx, sysModel.AdminUserToken{
		Id:      2,
		RoleId:  2,
		DepId:   1,
		RootTyp: 0,
	}); err != nil {
		t.Errorf("%v \n", err)
	} else {
		tests.PrintBeautifyJSON(rs)
	}
}

// 创建一个角色
func Test_RoleAdd(t *testing.T) {
	if err := srv.RoleAdd(ctx, &sysModel.RoleAddReq{
		Typ:      sysModel.ALL,
		RoleName: "开发人员",
		RoleCode: "devleporter",
		Status:   enum.ABLE,
	}); err != nil {
		t.Errorf("%v \n", err)
	}
}

// 角色分配权限
func Test_RoleBindAction(t *testing.T) {
	if err := srv.RoleBindAction(ctx, &sysModel.RoleBindActionReq{
		RoleId: 2,
		Ids:    []int64{1, 2, 12, 13, 14, 15},
	}); err != nil {
		t.Errorf("%v \n", err)
	}
}

// 角色分配权限
func Test_UserActionExist(t *testing.T) {
	if exist, err := srv.UserActionExist(&sysModel.AdminUserToken{
		Id:     2,
		RoleId: 2,
		DepId:  1,
	}, "/admin/v1/dep/treeList"); err != nil {
		t.Errorf("%v \n", err)
	} else {
		tests.PrintBeautifyJSON(exist)
	}
}

func Test_Cache(t *testing.T) {
	str := "1111"
	rs := enum.ToJson(str)
	fmt.Printf("%v\n", rs)

	var newStr string
	if err := enum.ToObject(rs, &newStr); err != nil {
		t.Error(err)
		return
	}
	fmt.Printf("%v\n", newStr)
}
