package sysModel

import (
	"gitee.com/jokces/kit/enum"
	"gitee.com/jokces/kit/typ"
	"strings"
)

type MenusListSelectedReq struct {
	MenuName string            `json:"menuName"       form:"menuName"`                    //菜单名称
	Status   enum.CommonStatus `json:"status"         form:"status"`                      //状态; 1-启用，2-停用
	MenuType string            `json:"menuType"       form:"menuType"`                    //菜单类型; M-目录,C-菜单
	GroupId  string            `json:"groupId"        form:"groupId"  binding:"required"` //分组ID
}

func (qms *MenusListSelectedReq) GetMenuTypes() []string {
	if qms.MenuType != "" {
		split := strings.Split(qms.MenuType, ",")
		return split
	}
	return []string{"M", "C"}
}

type MenuAddReq struct {
	ParentId   int64             `json:"parentId"        form:"parentId"`                           //上级菜单
	GroupId    string            `json:"groupId"         form:"groupId"         binding:"required"` //分组的ID
	MenuName   string            `json:"menuName"        form:"menuName"        binding:"required"` //菜单名称
	Title      string            `json:"title"           form:"title"`                              //菜单描述
	Icon       string            `json:"icon"            form:"icon"`                               //菜单icon
	Sequence   int32             `json:"sequence"        form:"sequence"`                           //排序号
	Status     enum.CommonStatus `json:"status"          form:"status"          binding:"required"` //状态; 1-启用，2-停用
	MenuType   MenuType          `json:"menuType"        form:"menuType"`                           //菜单类型; M-目录,C-菜单，B-按钮
	Component  string            `json:"component"       form:"component"`                          //组件
	Show       int32             `json:"show"            form:"show"`                               //是否显示; 1-显示,2-隐藏
	Permission string            `json:"permission"      form:"permission"      binding:"required"` //权限标志
}

type MenuEditReq struct {
	Id int64 `json:"id"          form:"id"      binding:"required,min=1"`
	MenuAddReq
}

type ActionListQueryReq struct {
	*typ.PageReq
	Status  enum.CommonStatus `json:"status"   form:"status"`  //状态; 1-启用，2-停用
	ApiName string            `json:"apiName"  form:"apiName"` //资源名称
	GroupId string            `json:"groupId"  form:"groupId"` //分组的ID
}

func (qr *ActionListQueryReq) GetPage() *typ.PageReq {
	if qr.PageReq != nil {
		return qr.PageReq
	}
	return nil
}

type ActionAddDocReq struct {
	Docs    string `json:"docs"            form:"docs"            binding:"required"` //json数据
	GroupId string `json:"groupId"         form:"groupId"         binding:"required"` //分组的ID
	Prefix  string `json:"prefix"          form:"prefix"`                             //过滤的API请求
	Expel   string `json:"expel"           form:"expel"`                              //排除的地址，多个用逗号隔开
}

type ActionEditReq struct {
	Id      int64             `json:"id"              form:"id"`
	GroupId string            `json:"groupId"         form:"groupId"         binding:"required"` //分组的ID
	ApiName string            `json:"apiName"         form:"apiName"         binding:"required"` //接口名称
	Api     string            `json:"api"             form:"api"             binding:"required"` //api地址
	Status  enum.CommonStatus `json:"status"          form:"status"`                             //状态; 1-启用，2-停用
}

type BindMenuWithActionReq struct {
	MenuId int64   `json:"menuId"   form:"menuId"    binding:"required"` //菜单ID
	Ids    []int64 `json:"ids"      form:"ids"       binding:"required"` //资源集合
}

type RoleListPageReq struct {
	*typ.PageReq
	RoleName string            `json:"roleName"      form:"roleName"` //角色名称
	Status   enum.CommonStatus `json:"status"        form:"status"`   //状态; 1-启用，2-停用
}

func (qr *RoleListPageReq) GetPage() *typ.PageReq {
	if qr.PageReq != nil {
		return qr.PageReq
	}
	return nil
}

type RoleAddReq struct {
	Typ      RoleType          `json:"typ"        form:"typ"          binding:"required"` //数据权限类型
	RoleName string            `json:"roleName"   form:"roleName"     binding:"required"` //用户名称
	RoleCode string            `json:"roleCode"   form:"roleCode"     binding:"required"` //角色编码
	Status   enum.CommonStatus `json:"status"     form:"status"`                          //状态; 1-启用，2-停用
}

type RoleEditReq struct {
	Id int64 `json:"id"         form:"id"      binding:"required,min=1"` //id
	RoleAddReq
}

type RoleActionResp struct {
	Id        int64  `json:"id"`        //id
	MenuName  string `json:"menuName"`  //资源名称
	GroupName string `json:"groupName"` //分组的名称
	GroupId   string `json:"groupId"`   //分组id
	Pid       int64  `json:"pid"`       //上级ID
}

type RoleActionsResp struct {
	ActList []*RoleActionResp `json:"actList"` //当前登录用户所有的菜单资源
	Checked []int64           `json:"checked"` //所选用户的已拥有的菜单资源
}

type MenuGroup struct {
	GroupName string            `json:"groupName"` //分组名称
	GroupId   string            `json:"groupId"`   //分组id
	ActList   []*RoleActionResp `json:"actList"`   //当前登录用户所有的菜单资源
	Checked   []int64           `json:"checked"`   //所选用户的已拥有的菜单资源
}

type RoleBindActionReq struct {
	RoleId int64   `json:"roleId"    form:"roleId"` //角色ID
	Ids    []int64 `json:"ids"       form:"ids"`    //所有的资源
}

type SwaggerJSON struct {
	BasePath string                   `json:"basePath"`
	Paths    map[string]SwaggerMethod `json:"paths"`
}
type SwaggerMethod map[string]struct {
	Summary string `json:"summary"`
}
