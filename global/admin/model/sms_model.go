package sysModel

import "gitee.com/jokces/kit/enum"

type MsgType int32

const (
	MsgTypeSms   MsgType = iota + 1 //1- 短信
	MsgTypeEmail                    //2- 邮件
)

const (
	Code                string = "CODE"    //表示验证码内别的处理
	MessageEmailCodeTtl        = "MSG_EM"  //发送消息的组合处理
	MessageSmsCodeTtl          = "MSG_SMS" //验证码的验证消息
	MessageMax                 = "MSG_MAX" //一天最大发送短信的次数
)

type MessageRecords struct {
	Id         int64         `json:"id"          xorm:"not null pk autoincr BIGINT(20)"`
	ToUser     string        `json:"toUser"      xorm:"not null VARCHAR(60) index comment('接受消息方： 手机号或者邮件地址')"` //接受消息方
	Content    string        `json:"content"     xorm:"not null text  comment('邮件内容')"`                         //邮件内容
	RandCode   string        `json:"randCode"    xorm:"not null VARCHAR(32) index comment('随机模版ID，最大32位')"`     //随机模版ID
	BizCode    string        `json:"bizCode"     xorm:"not null VARCHAR(32) index  comment('业务模型代码，最大32位')"`    //随机模版ID
	BizInfo    string        `json:"bizInfo"     xorm:"null VARCHAR(64) comment('三方返回的ID')"`                    //随机模版ID
	MsgType    MsgType       `json:"msgType"     xorm:"null int default 1 comment('类型; 1-短信,2-邮件')"`            //类型; 类型; 1-短信,2-邮件
	SendStatus int32         `json:"sendStatus"  xorm:"null int default 1 comment('类型; 1-短信,2-邮件')"`            //成功状态; 1-成功,2-失败
	SendTime   int64         `json:"sendTime"    xorm:"null BIGINT(20) default 0 comment('发送时间')"`
	CreatedAt  enum.JsonTime `json:"createdAt"   xorm:"created"`
}
