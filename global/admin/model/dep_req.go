package sysModel

import (
	"gitee.com/jokces/kit/enum"
)

// DepTreeReq 部门
type DepTreeReq struct {
	DepName string            `json:"depName"        form:"depName"` //部门名称
	Status  enum.CommonStatus `json:"status"         form:"status"`  //状态; 1-启用，2-停用
}
type DepAddReq struct {
	ParentId int64             `json:"parentId"      form:"depName"      binding:"required,min=1"` //上级部门ID
	DepName  string            `json:"depName"       form:"depName"      binding:"required"`       //部门名称
	Status   enum.CommonStatus `json:"status"        form:"status"       binding:"min=1,max=2"`    //状态; 1-启用，2-停用
}
type DepEditReq struct {
	Id      int64             `json:"id"             form:"id"          binding:"required,min=1"` //部门ID
	DepName string            `json:"depName"        form:"depName"     binding:"required"`       //部门名称
	Status  enum.CommonStatus `json:"status"         form:"status"`                               //状态; 1-启用，2-停用
}
