package sysModel

import (
	"gitee.com/jokces/kit/enum"
)

type SysDep struct {
	Id          int64             `json:"id"         xorm:"not null pk autoincr BIGINT(20)"`
	ParentId    int64             `json:"parentId"   xorm:"null BIGINT(20) default 0 comment('上级ID')"`             //上级ID
	DepName     string            `json:"depName"    xorm:"not null VARCHAR(60) unique(dep_name) comment('部门名称')"` //部门名称
	DepCode     string            `json:"-"          xorm:"null VARCHAR(64) comment('部门编码')"`                      //部门编码
	Status      enum.CommonStatus `json:"status"     xorm:"null  int default 1 comment('状态; 1-启用，2-停用')"`          //状态; 1-启用，2-停用
	UpdatedAt   enum.JsonTime     `json:"updatedAt"  xorm:"updated"`
	CreatedAt   enum.JsonTime     `json:"createdAt"  xorm:"created"`
	IsDel       int32             `json:"-"          xorm:"null int default 0 comment('状态; 0：正常，1-删除')"`
	Children    []*SysDep         `json:"children"   xorm:"-"`
	LikeDepName string            `json:"-"          xorm:"-"`
	LikeDepCode string            `json:"-"          xorm:"-"`
}
