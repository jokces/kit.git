package sysModel

import (
	"crypto/md5"
	"encoding/hex"
	"gitee.com/jokces/kit/enum"
	"strings"
)

const (
	AccountTable      = "user_account"
	AccountThirdTable = "user_third_account"
)

type AccountSession struct {
	Id         int64             `json:"id"               xorm:"not null pk autoincr BIGINT(20)"`                  //用户ID
	Uid        string            `json:"uid"              xorm:"not null VARCHAR(32) unique(uid) comment('uid')"`  //用户UID
	InviteId   int64             `json:"inviteId"         xorm:"null BIGINT(20) default 0 index comment('邀请人ID')"` //用户邀请人ID
	InviteCode string            `json:"inviteCode"       xorm:"null VARCHAR(12) comment('邀请码')"`                  //邀请码
	Email      string            `json:"email"            xorm:"null VARCHAR(64) unique(email) comment('邮箱地址')"`   //邮箱
	Mobile     string            `json:"mobile"           xorm:"null VARCHAR(20) unique(mobile) comment('手机号')"`   //手机号
	UserName   string            `json:"userName"         xorm:"null VARCHAR(20) unique(user_name) comment('账号')"` //账号
	UserStatus enum.CommonStatus `json:"userStatus"       xorm:"null int default 1 comment('状态; 1-启用，2-停用')"`      //启用停用
	Pass       string            `json:"-"                xorm:"null VARCHAR(32) comment('密码')"`                   //密码
	Salt       string            `json:"-"                xorm:"null VARCHAR(6) comment('盐')"`                     //盐
}

func (a *AccountSession) OneParamCheck() bool {
	return a.Id < 0 && a.InviteCode == "" && a.Email == "" && a.Mobile == "" && a.Uid == ""
}
func (a *AccountSession) CheckPass(pass string) bool {
	//密码的算法控制，md5（密码+盐）
	buffer := pass + a.Salt
	h := md5.New()
	h.Write([]byte(buffer))
	password := hex.EncodeToString(h.Sum(nil))
	if strings.ToLower(a.Pass) == strings.ToLower(password) {
		return true
	}
	return false
}

// AccountThird 用户三方账号的处理
type AccountThird struct {
	Id      int64  `json:"id"`
	AppId   string `json:"appId"`   //应用类型Id,小程序，公众号，等等
	UserId  int64  `json:"userId"`  //用户账号ID
	UnionId string `json:"unionId"` // 平台唯一的ID
	OpenId  string `json:"openId"`  //主题openId
}

// TODO 这个到时候废弃
type UserAccount struct {
	AccountSession `xorm:"extends"`
	LastLoginTime  enum.JsonUnixTime `json:"lastLoginTime"   xorm:"null BIGINT(20) default 0 comment('最后登陆的时间')"`
	UpdatedAt      enum.JsonTime     `json:"updatedAt"       xorm:"updated"`
	CreatedAt      enum.JsonTime     `json:"createdAt"       xorm:"created"`
}
