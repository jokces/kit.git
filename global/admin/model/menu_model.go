package sysModel

import (
	"gitee.com/jokces/kit/enum"
)

type MenuType string

const (
	Root MenuType = "M" //目录
	Menu MenuType = "C" //菜单
	Btn  MenuType = "B" //按钮
)

type SysMenu struct {
	Id          int64             `json:"id"                    xorm:"not null pk autoincr BIGINT(20)"`
	ParentId    int64             `json:"parentId"              xorm:"null BIGINT(20) default 0 comment('上级ID')"`                 //上级ID
	GroupId     enum.DicEnum      `json:"groupId"               xorm:"null VARCHAR(60)  comment('分组的ID')"`                        //菜单分组的ID，主要用于client_ids 权限的处理
	MenuName    string            `json:"menuName"              xorm:"not null VARCHAR(60)  comment('菜单名称')"`                     //菜单名称
	Title       string            `json:"title"                 xorm:"not null VARCHAR(60)  comment('菜单描述')"`                     //菜单描述
	Icon        string            `json:"icon"                  xorm:"not null VARCHAR(200) comment('icon')"`                     //icon
	Sequence    int32             `json:"sequence"              xorm:"null int default 1 comment('顺序，采用越大顺序越靠前')"`                //排序号
	Status      enum.CommonStatus `json:"status"                xorm:"null int default 1 comment('状态; 1-启用，2-停用')"`               //启用停用
	MenuType    MenuType          `json:"menuType"              xorm:"not null VARCHAR(1) index comment('菜单类型; M-目录,C-菜单，B-功能')"` //菜单类型; M-目录,C-菜单,B-按钮
	Show        int32             `json:"show"                  xorm:"null int default 1 comment('是否显示; 1-显示,2-隐藏')"`             //是否显示; 1-显示,2-隐藏
	Component   string            `json:"component"             xorm:"null VARCHAR(200) comment('组件')"`                           //组件
	Permission  string            `json:"permission"            xorm:"null VARCHAR(200) comment('权限标志')"`                         //权限标志
	UpdatedAt   enum.JsonTime     `json:"updatedAt"             xorm:"updated"`
	CreatedAt   enum.JsonTime     `json:"createdAt"             xorm:"created"`
	Children    []*SysMenu        `json:"children"              xorm:"-"`
	MenuTypeArr []string          `json:"-"                     xorm:"-"` //菜单的条件
}

type SysAction struct {
	Id        int64             `json:"id"                   xorm:"not null pk autoincr BIGINT(20)"`
	GroupId   enum.DicEnum      `json:"groupId"              xorm:"null VARCHAR(60)  comment('分组的ID')"`                 //分组ID
	ApiName   string            `json:"apiName"              xorm:"not null VARCHAR(60) comment('接口名称')"`               //接口名称
	Api       string            `json:"api"                  xorm:"not null VARCHAR(200) unique(api) comment('请求的地址')"` //api地址
	Code      string            `json:"code"                 xorm:"null VARCHAR(200)  comment('编码:  t:功能的方式')"`         //功能编码，后面按钮权限使用
	Status    enum.CommonStatus `json:"status"               xorm:"null  int default 1 comment('状态; 1-启用，2-停用')"`       //状态; 1-启用，2-停用
	UpdatedAt enum.JsonTime     `json:"updatedAt"            xorm:"updated"`
	CreatedAt enum.JsonTime     `json:"createdAt"            xorm:"created"`
}

type SysActionBo struct {
	Id      int64  `json:"id"`
	ApiName string `json:"apiName"` //接口名称
	Api     string `json:"api"`     //api地址
	Code    string `json:"code"`    //功能编码，后面按钮权限使用
}

type SysRole struct {
	Id           int64             `json:"id"         xorm:"not null pk autoincr BIGINT(20)"`
	RoleName     string            `json:"roleName"   xorm:"not null VARCHAR(60) comment('角色名称')"`                    // 角色名称
	RoleCode     string            `json:"key"   xorm:"not null VARCHAR(60) comment('角色编码')"`                         // 角色编码
	IsRoot       int32             `json:"-"          xorm:"not null int default 0 comment('1-root,0-其他)"`            //是否为超级管理员
	Typ          RoleType          `json:"typ"        xorm:"not null int default 1 comment('1-所有权限,2-下级权限,3-本部门权限')"` //权限类型
	Status       enum.CommonStatus `json:"status"     xorm:"null int default 1 comment('状态;1-启用，2-停用')"`              //状态; 1-启用，2-停用
	IsDel        int32             `json:"-"          xorm:"null int default 0 comment('状态; 0：正常，1-删除')"`
	UpdatedAt    enum.JsonTime     `json:"updatedAt"  xorm:"updated"`
	CreatedAt    enum.JsonTime     `json:"createdAt"  xorm:"created"`
	LikeRoleName string            `json:"-"          xorm:"-"` // 唯一的角色名
}

type RoleDat struct {
	Typ RoleType `json:"typ"` //数据权限类型
	Val string   `json:"val"` //描述
}

// SysMenuRole 菜单角色
type SysMenuRole struct {
	Id     int64 `xorm:"not null pk autoincr BIGINT(20)"`
	RoleId int64 `xorm:"not null BIGINT(20) index comment('角色ID')"`
	MenuId int64 `xorm:"not null BIGINT(20) index comment('菜单ID')"`
}

// SysMenuAction 菜单资源
type SysMenuAction struct {
	Id       int64 `xorm:"not null pk autoincr BIGINT(20)"`
	ActionId int64 `xorm:"not null BIGINT(20) index comment('资源ID')"`
	MenuId   int64 `xorm:"not null BIGINT(20) index comment('菜单ID')"`
}
