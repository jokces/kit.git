package sysModel

import (
	"fmt"
	"gitee.com/jokces/kit/errc"
	"gitee.com/jokces/kit/typ"
)

const (
	AdminType string = "admin"
	ApiType   string = "api"
)

func ValidateType(appType string) bool {
	return appType == AdminType || appType == ApiType
}

type AccessTokenResp struct {
	AccessToken  string `json:"accessToken,omitempty"`  //请求授权accessToken
	RefreshToken string `json:"refreshToken,omitempty"` //刷新的token
	ExpiresIn    int64  `json:"expiresIn,omitempty"`    //过期时间：秒(s)
}

type AccessTokenReq struct {
	ClientId     string `json:"clientId"                  form:"clientId"`     //Oauth2客户端ID
	ClientSecret string `json:"clientSecret"              form:"clientSecret"` //Oauth2客户端秘钥
	GrantType    string `json:"grantType"                 form:"grantType"`    //授权模式类型(password,refresh_token,client_credentials,authorization_code,mobile
	RefreshToken string `json:"refreshToken"              form:"refreshToken"` //刷新token
	ResponseType string `json:"responseType"              form:"responseType"` //请求的体
	UserName     string `json:"userName"                  form:"userName"`     //用户名
	Phone        string `json:"phone"                     form:"phone"`        //手机号码
	Email        string `json:"email"                     form:"email"`        //邮箱
	Password     string `json:"password"                  form:"password"`     //用户密码
	SessionId    string `json:"sessionId"                 form:"sessionId"`    //用户名随机的KEY
	Code         string `json:"code"                      form:"code"`         //code
	AppType      string `json:"-"                         form:"-"`            //app类型
}

func (a *AccessTokenReq) AuthClientCredentialsParam() error {
	if a.ClientId == "" {
		return errc.ErrParamInvalid.MultiMsg("ClientId Need")
	}
	if a.Code == "" {
		return errc.ErrParamInvalid.MultiMsg("Code Need")
	}
	return nil
}

// AuthCodeParam 验证code的获取处理
func (a *AccessTokenReq) AuthCodeParam() error {
	if a.GrantType == "" {
		return errc.ErrParamInvalid.MultiMsg("GrantType Need")
	}
	if a.ResponseType != RespCode.ToString() {
		return errc.ErrParamInvalid.MultiMsg("ResponseType Need")
	}
	return nil
}
func (a *AccessTokenReq) AuthPassParam() error {
	if a.UserName == "" && a.Email == "" && a.Phone == "" {
		return errc.ErrParamInvalid.MultiMsg("UserName Need")
	}
	if a.Password == "" {
		return errc.ErrParamInvalid.MultiMsg("Password Need")
	}
	return nil
}
func (a *AccessTokenReq) AuthMobileParam() error {
	if a.Phone == "" {
		return errc.ErrParamInvalid.MultiMsg("Phone Need")
	}
	if a.SessionId == "" {
		return errc.ErrParamInvalid.MultiMsg("SessionId Need")
	}
	if a.Code == "" {
		return errc.ErrParamInvalid.MultiMsg("Code Need")
	}
	return nil
}
func (a *AccessTokenReq) AuthEmailParam() error {
	if a.Email == "" {
		return errc.ErrParamInvalid.MultiMsg("Email Need")
	}
	if a.SessionId == "" {
		return errc.ErrParamInvalid.MultiMsg("SessionId Need")
	}
	if a.Code == "" {
		return errc.ErrParamInvalid.MultiMsg("Code Need")
	}
	return nil
}

func (a *AccessTokenReq) DoesAdmin() bool {
	return a.AppType == AdminType
}

// ValidateClient 客户端进行验证
func (a *AccessTokenReq) ValidateClient() bool {
	return true
}

type OauthClientAddReq struct {
	ResourceIds          string    `json:"resourceIds"                form:"resourceIds"`          //资源集合ID
	Authorities          string    `json:"authorities"                form:"authorities"`          //权限集合
	ClientSecret         string    `json:"clientSecret"               form:"clientSecret"`         //客户端秘钥,如果不传则系统生成
	Scope                ScopeType `json:"scope"                      form:"scope"`                //授权范围；
	GrantTypes           GrantType `json:"grantTypes"                 form:"grantTypes"`           //授权类型;分割:password,refresh_token,authorization_code,client_credentials,mobile
	RedirectUri          string    `json:"redirectUri"                form:"redirectUri"`          //回调地址
	AccessTokenValidity  int32     `json:"accessTokenValidity"        form:"accessTokenValidity"`  //令牌有效时间s
	RefreshTokenValidity int32     `json:"refreshTokenValidity"       form:"refreshTokenValidity"` //刷新令牌的有效时间s
	AdditionalInfo       string    `json:"additionalInfo"             form:"additionalInfo"`
	ClientName           string    `json:"clientName"                 form:"authorities"           binding:"required"` //客户端名称
	ClientId             string    `json:"clientId"                   form:"clientId"              binding:"required"` //客户端ID
}
type OauthClientUpdateReq struct {
	CommonIdReq
	OauthClientAddReq
}

func (c *OauthClientUpdateReq) Key() string {
	return fmt.Sprintf("%v:%v", "cli", c.ClientId)
}

type OauthQueryReq struct {
	*typ.PageReq
	ClientName string `json:"clientName"                 form:"clientName"` //客户端名称
	ClientId   string `json:"clientId"                   form:"clientId"`   //客户端ID
}

type OauthQueryResp struct {
	ClientName string `json:"clientName"                 form:"authorities"` //客户端名称
	ClientId   string `json:"clientId"                   form:"clientId"`    //客户端ID
}

type ReTokenReq struct {
	RefreshToken string `json:"refreshToken"             form:"refreshToken"       binding:"required"` //刷新的TOKEN
	AppType      string `json:"-"`                                                                     //admin,api
}

// ReTokenCache 刷新TOKEN的缓存数据
type ReTokenCache struct {
	UserId             int64  `json:"userId"`             //用户ID
	Token              string `json:"token"`              //当前登录的token数据
	RefreshToken       string `json:"-"`                  //刷新的TOKEN
	ClientId           string `json:"clientId"`           //客户端的ID
	AccessTokenExpire  int32  `json:"accessTokenExpire"`  //令牌有效时间s
	RefreshTokenExpire int32  `json:"refreshTokenExpire"` //刷新令牌的有效时间s
	AppType            string `json:"appType"`            //admin,api
}

// AuthorizationCache CODE对应的缓存数据
type AuthorizationCache struct {
	UserId  int64  `json:"userId"`  //用户ID
	AppType string `json:"appType"` //admin,api
	*AccessTokenReq
}

func (r *ReTokenCache) NumberAppType() int32 {
	if r.AppType == AdminType {
		return 1
	}
	return 2
}

// AuthorizationCodeResp 返回的认证CODE的处理
type AuthorizationCodeResp struct {
	Code string `json:"code,omitempty"` //授权CODE
}

type RedisTokenResp struct {
	Token    string `json:"token"`    //token
	UserId   int64  `json:"userId"`   //用户ID
	Uid      string `json:"uid"`      //用户UID
	Email    string `json:"email"`    //邮箱
	Mobile   string `json:"mobile"`   //手机号
	UserName string `json:"userName"` //账号
}

type BaseSession struct {
	Id int64 `json:"id"` //用户ID
}

type ClientAddResourceReq struct {
	CommonIdReq
	ResourceIds string `json:"resourceIds"        form:"resourceIds"    binding:"required"` //资源集合ID，目前只支持单个分组
}

type CacheClient struct {
	ClientId string
}

func (c *CacheClient) Key() string {
	return fmt.Sprintf("%v:%v", "cli", c.ClientId)
}

type CheckTokenResp struct {
	ExpiresAt int64 `json:"expiresAt"` //过期的时间
	NowTime   int64 `json:"nowTime"`   //服务器的当前时间
}

type CheckTokenReq struct {
	Token string `json:"token"   form:"token"    binding:"required"` //当前服务的token
}
