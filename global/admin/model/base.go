package sysModel

import (
	"gitee.com/jokces/kit/enum"
	"gitee.com/jokces/kit/global/glconf"
)

const (
	TokenHeader       = "AX-Access-Token"
	ClientHeader      = "AX-Client-Token" // 客户断的header的处理
	AdminActionAble   = "AdminActionAble" // 所有的功能列表
	AdminActions      = "ADMIN_ACTIONS"   // 管理后台用户的权限
	AdminRefreshToken = "AR"              // /admin 刷新token前缀
	ApiRefreshToken   = "AT"              // /api 刷新token前缀
	AdminToken        = "TA"              // /admin token前缀
	ApiToken          = "TI"              // /api token前缀
)

type CommonIdReq struct {
	Id int64 `json:"id"       form:"id"         binding:"required,min=1"` //ID
}
type CommonDisabledReq struct {
	Id     int64             `json:"id"           binding:"required"`                                      //id
	Status enum.CommonStatus `json:"status"       form:"status"            binding:"required,gte=1,lte=2"` //状态; 1-启用，2-停用
}
type CommonStatusReq struct {
	Status enum.CommonStatus `json:"status"       form:"status"            binding:"required,gte=1,lte=2"` //状态; 1-启用，2-停用
}

func CheckCommonStatus(status enum.CommonStatus) bool {
	return status == enum.ABLE || status == enum.DISABLED
}

// 用来初始化对应的数据
// 目前采用集中式的处理，如果后期需要单独处理成服务，那么就直接根据这个global拆分成对应的服务就行了
func init() {

	//系统部分的功能
	glconf.AddSyncModels(new(SysDep))
	glconf.AddSyncModels(new(SysMenu))
	glconf.AddSyncModels(new(SysAction))
	glconf.AddSyncModels(new(SysRole))
	glconf.AddSyncModels(new(SysUser))
	glconf.AddSyncModels(new(SysMenuRole))
	glconf.AddSyncModels(new(SysMenuAction))

	glconf.AddSyncModels(new(SysFileRecord))
	glconf.AddSyncModels(new(SysParam))
	glconf.AddSyncModels(new(SysDic))
	glconf.AddSyncModels(new(SysArea))
	glconf.AddSyncModels(new(SysOperateLog))
	glconf.AddSyncModels(new(SysClassify))

	//授权部分的功能
	glconf.AddSyncModels(new(SysOauthClient))

	//基础用户相关的功能
	glconf.AddSyncModels(new(MessageRecords))

	//TODO： 这个后面需要注释掉，放开的操作、
	glconf.AddSyncModels(new(UserAccount))
}
