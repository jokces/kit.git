package sysModel

import (
	"gitee.com/jokces/kit/enum"
	"gitee.com/jokces/kit/typ"
)

type FileReq struct {
	FileName string `json:"fileName" form:"fileName" binding:"required"`
}

type FileModelResp struct {
	Url       string `json:"url"`
	Timestamp int64  `json:"timestamp"`
}

type ImageCodeResp struct {
	Code   string `json:"code"`
	CodeId string `json:"codeId"`
}

// ParamListReq 参数
type ParamListReq struct {
	typ.PageReq
	Key    string            `json:"key"      form:"key"`    //参数key
	Name   string            `json:"name"     form:"name"`   //参数名称
	Status enum.CommonStatus `json:"status"   form:"status"` //状态; 1-启用，2-停用
}
type ParamEditReq struct {
	Id     int64             `json:"id"     form:"id"`                            //部门ID
	Key    string            `json:"key"    form:"key"     binding:"required"`    //参数key
	Name   string            `json:"name"   form:"name"    binding:"required"`    //参数名称
	Value  string            `json:"value"  form:"value"   binding:"required"`    //参数值
	Memo   string            `json:"memo"   form:"memo"`                          //参数值描述
	Status enum.CommonStatus `json:"status" form:"status"  binding:"min=1,max=2"` //状态; 1-启用，2-停用
}

type DicListReq struct {
	typ.PageReq
	Key    string            `json:"key"              form:"key"`    //参数key
	Name   string            `json:"name"             form:"name"`   //参数名称
	Status enum.CommonStatus `json:"status"           form:"status"` //状态; 1-启用，2-停用
}

func (d *DicListReq) PageGet() *typ.PageReq {
	if d.Page > 0 && d.Size > 0 {
		return &typ.PageReq{
			Page: d.Page,
			Size: d.Size,
		}
	}
	return nil
}

type AreaListReq struct {
	ParentId int64 `json:"parentId"   form:"parentId"` //上级ID，如果parentId == 0 && All = false 查询省
	All      bool  `json:"all"        form:"all"`      // 是否所有数据 All = TRUE，则表示查询所有的数据
}

type AreaListResp struct {
	Id       int64  `json:"id"`       //ID
	ParentId int64  `json:"parentId"` //上级ID
	AreaName string `json:"areaName"` //名称
}

type OptListReq struct {
	typ.PageReq
	typ.TimeStringReq
	Opt      string `json:"opt"             form:"opt"`
	TraceId  string `json:"traceId"         form:"traceId"`  //traceId
	UserName string `json:"userName"        form:"userName"` //操作人
}

type OpinionListReq struct {
	typ.PageReq
	typ.TimeStringReq
	UserName string `json:"userName" form:"userName"` //用户名称
	DicId    string `json:"dicId" form:"dicId"`       //分类ID
}

type OpinionAddReq struct {
	UserName string `json:"userName"   form:"userName"    binding:"required"` //用户名称
	Content  string `json:"content"    form:"content"     binding:"required"` //意见内容
	DicId    string `json:"dicId"      form:"dicId"       binding:"required"` //分类ID
}

type DicVo struct {
	Key   string `json:"key"`   //字典Key
	Value string `json:"value"` //字典值
}

type DicReVo struct {
	DicVo
	GroupKey string `json:"groupKey"` //分组的KEY
	Extra    string `json:"extra"`    //附加参数
}

type ClassifyListReq struct {
	*typ.PageReq
	ParentId int64             `json:"parentId"      form:"parentId"` //上级ID
	Name     string            `json:"name"          form:"parentId"` //参数名称
	Code     string            `json:"code"          form:"code"`     //编码
	Status   enum.CommonStatus `json:"status"        form:"status"`   //状态; 1-启用，2-停用
}

type ClassifyAddReq struct {
	ParentId int64             `json:"parentId"      form:"parentId"`                              //上级ID
	Name     string            `json:"name"          form:"name"            binding:"required"`    //参数名称
	Status   enum.CommonStatus `json:"status"        form:"status"          binding:"min=1,max=2"` //状态; 1-启用，2-停用
	Extra    string            `json:"extra"         form:"extra"`                                 // 额外的参数值
}

type ClassifyEditReq struct {
	Id     int64             `json:"id"            form:"id"              binding:"required,min=1"` //ID
	Name   string            `json:"name"          form:"name"            binding:"required"`       //参数名称
	Status enum.CommonStatus `json:"status"        form:"status"          binding:"min=1,max=2"`    //状态; 1-启用，2-停用
	Extra  string            `json:"extra"         form:"extra"`                                    // 额外的参数值
}
