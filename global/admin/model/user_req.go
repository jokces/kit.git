package sysModel

import (
	"gitee.com/jokces/kit/enum"
	"gitee.com/jokces/kit/typ"
)

type UserListReq struct {
	*typ.PageReq
	UserName string            `json:"userName"       form:"userName"` //用户名称
	Phone    string            `json:"phone"          form:"phone"`    //用户名称
	Email    string            `json:"email"          form:"email"`    //用户名称
	DepId    int64             `json:"depId"          form:"depId"`    //部门ID
	Status   enum.CommonStatus `json:"status"         form:"status"`   //状态; 1-启用，2-停用
}

func (qr *UserListReq) GetPage() *typ.PageReq {
	if qr.PageReq != nil {
		return qr.PageReq
	}
	return nil
}

type UserAddReq struct {
	UserName string `json:"userName"     form:"userName"    binding:"required"`        //用户名称
	Nickname string `json:"nickName"     form:"nickName"    binding:"required"`        //昵称
	Phone    string `json:"phone"        form:"phone"`                                 //手机号
	Email    string `json:"email"        form:"email"`                                 //邮箱
	DepId    int64  `json:"depId"        form:"depId"       binding:"required"`        //部门ID
	RoleId   int64  `json:"roleId"       form:"roleId"      binding:"required"`        //角色ID
	Pass     string `json:"pass"         form:"pass"        binding:"required,len=32"` //登陆密码
}

type UserRestPassReq struct {
	Id      int64  `json:"id"     form:"id"      binding:"required,min=1"`    //id
	Pass    string `json:"pass"   form:"pass"      binding:"required,len=32"` //登陆密码,需要用md5的值传入进来
	OldPass string `json:"-"`                                                 //原密码,需要用md5的值传入进来
}

type UserRestMyPassReq struct {
	Pass    string `json:"pass"       form:"pass"         binding:"required,len=32"` //登陆密码,需要用md5的值传入进来
	OldPass string `json:"oldPass"    form:"oldPass"      binding:"required,len=32"` //原密码,需要用md5的值传入进来
}

type UserBindRoleReq struct {
	Id     int64 `json:"id"       form:"roleId"      binding:"required,min=1"` //id
	RoleId int64 `json:"roleId"   form:"roleId"`                               //角色ID
}

type UserRestInfoReq struct {
	Nickname string `json:"nickname"     form:"nickname"    binding:"required"` //昵称
	Phone    string `json:"phone"        form:"phone"       binding:"required"` //手机号
	Email    string `json:"email"        form:"email"       binding:"required"` //邮箱
}

// UserInfoResp 用户详情
type UserInfoResp struct {
	Menus       []*UserMenus `json:"menus"`       //菜单列表
	Permissions []string     `json:"permissions"` //权限资源列表
	Roles       []string     `json:"roles"`       //角色列表
	UserName    string       `json:"userName"`    //用户昵称
	NickName    string       `json:"nickName"`    //昵称
	DepName     string       `json:"depName"`     //部门名称
	Phone       string       `json:"phone"`       //手机号
	Email       string       `json:"email"`       //邮箱
}

type UserMenus struct {
	Id         int64    `json:"id"`         //ID
	PId        int64    `json:"pid"`        //上级ID
	MenuName   string   `json:"menuName"`   //菜单名称
	Title      string   `json:"title"`      //菜单描述
	Icon       string   `json:"icon"`       //icon
	Sequence   int32    `json:"sequence"`   //排序号
	MenuType   MenuType `json:"menuType"`   //菜单类型; M-目录,C-菜单,B-功能
	Show       int32    `json:"show"`       //是否显示; 1-显示,2-隐藏
	Component  string   `json:"component"`  //组件
	Permission string   `json:"permission"` //权限标记
}

type AdminLoginReq struct {
	UserName string `json:"userName"      form:"userName"       binding:"required"`
	Pass     string `json:"pass"          form:"pass"           binding:"required"`
	Code     string `json:"code"          form:"code"           binding:"required"` //验证码
	CodeId   string `json:"codeId"        form:"codeId"         binding:"required"` //验证码顺序号
	LoginIp  string `json:"-"`
}

type AdminLoginResp struct {
	UserName   string `json:"userName"`   //用户昵称
	Nickname   string `json:"nickname"`   //昵称
	DepName    string `json:"depName"`    //部门名称
	Token      string `json:"token"`      //token
	ExpireTime int64  `json:"expireTime"` //过期的时间
}

type UserEditReq struct {
	Id       int64  `json:"id"                  form:"id"             binding:"required,min=1"` //id
	Nickname string `json:"nickname"            form:"nickname"       binding:"required"`       //昵称
	RoleId   int64  `json:"roleId"              form:"roleId"         binding:"required"`       //角色ID
	Phone    string `json:"phone"               form:"phone"`                                   //手机号
	Email    string `json:"email"               form:"email"`                                   //邮箱
}
