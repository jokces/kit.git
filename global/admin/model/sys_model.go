package sysModel

import (
	"encoding/json"
	"gitee.com/jokces/kit/enum"
	"strconv"
	"time"
)

// SysFileRecord 文件记录
type SysFileRecord struct {
	Id           int64     `xorm:"not null pk autoincr BIGINT(20)"`
	FileName     string    `xorm:"null VARCHAR(200) index comment('文件名称')"`
	FileOriginal string    `xorm:"null VARCHAR(512) comment('原始文件名称')"`
	FileTemp     string    `xorm:"null VARCHAR(512) comment('原始文件地址')"`
	Url          string    `xorm:"null VARCHAR(512) comment('文件路径')"`
	Hex          string    `xorm:"null VARCHAR(512) comment('文件的hash')"`
	FileType     string    `xorm:"null VARCHAR(30)  comment('文件后缀')"`
	FileSize     int64     `xorm:"null  BIGINT(20) default 0 comment('文件大小')"`
	UpdatedAt    time.Time `xorm:"updated"`
	CreatedAt    time.Time `xorm:"created"`
}

type SysDic struct {
	Id        int32             `json:"id"        xorm:"NOT NULL PK AUTOINCR BIGINT(20)"`
	Key       string            `json:"key"       xorm:"NOT NULL VARCHAR(128) index COMMENT('参数名称')"`              //字典Key
	UniqueKey string            `json:"uniqueKey" xorm:"NOT NULL VARCHAR(64) unique(unique_keys) COMMENT('参数名称')"` //字典唯一的KEY
	Value     string            `json:"value"     xorm:"NOT NULL VARCHAR(256) COMMENT('参数值')"`                     //参数值
	Extra     string            `json:"extra"     xorm:"NOT NULL VARCHAR(256) COMMENT('参数值')"`                     // 额外的参数值
	Status    enum.CommonStatus `json:"status"    xorm:"NOT NULL int default 1 comment('状态; 1-启用，2-停用')"`          //
}

type SysArea struct {
	Id         int64  `json:"id"             xorm:"NOT NULL PK AUTOINCR BIGINT(20)"`
	AreaName   string `json:"areaName"       xorm:"NOT NULL VARCHAR(128) COMMENT('名称')"`              //名称
	ParentId   int64  `json:"parentId"       xorm:"null BIGINT(20) default 0 comment('上级ID')"`        //上级ID
	ShortName  string `json:"shortName"      xorm:"NOT NULL VARCHAR(128) COMMENT('简称')"`              //简称
	LevelType  int32  `json:"levelType"      xorm:"null int default 1 comment('级别')"`                 //级别
	AmapAdCode string `json:"amapAdCode"     xorm:"NOT NULL VARCHAR(50) COMMENT('高德地区码')"`            //高德地区码
	QqAdCode   string `json:"qqAdCode"       xorm:"NOT NULL VARCHAR(50) COMMENT('腾讯地图地区码')"`          //腾讯地图地区码
	AreaCode   string `json:"areaCode"       xorm:"NOT NULL VARCHAR(4) COMMENT('城市区号')"`              //城市区号
	ZipCode    string `json:"zipCode"        xorm:"NOT NULL VARCHAR(6) COMMENT('邮编')"`                //邮编
	MergerName string `json:"mergerName"     xorm:"NOT NULL VARCHAR(120) COMMENT('全名')"`              //全名
	MergerCode string `json:"mergerCode"     xorm:"NOT NULL VARCHAR(120) COMMENT('区域ID编码')"`          //区域ID编码
	Lng        string `json:"lng"            xorm:"NOT NULL VARCHAR(20) COMMENT('经度')"`               //经度
	Lat        string `json:"lat"            xorm:"NOT NULL VARCHAR(20) COMMENT('纬度')"`               //纬度
	PinYin     string `json:"pinYin"         xorm:"NOT NULL VARCHAR(30) COMMENT('拼音')"`               //拼音
	Remark     string `json:"remark"         xorm:"NOT NULL VARCHAR(255) COMMENT('备注')"`              //备注
	Region     string `json:"region"         xorm:"NOT NULL VARCHAR(32) DEFAULT '0' COMMENT('所属区域')"` //所属区域
}

// AreaVo 区域地址的处理
type AreaVo struct {
	Ids   string `json:"ids"`   //区域id，例如: 50010,52201,5220
	Value string `json:"value"` //区域值, 例如：重庆市,重庆市,渝中区
}

// SysParam 系统配置参数
type SysParam struct {
	Id        int64             `json:"id"        xorm:"NOT NULL PK AUTOINCR BIGINT(20)"`
	Key       string            `json:"key"       xorm:"VARCHAR(128) UNIQUE(U_KEY) COMMENT('参数名称')"` //参数key
	Name      string            `json:"name"      xorm:"VARCHAR(256) COMMENT('参数名称描述')"`             //参数名称
	Value     string            `json:"value"     xorm:"text COMMENT('参数值')"`                        //参数值
	Memo      string            `json:"memo"      xorm:"VARCHAR(512) COMMENT('参数值描述')"`
	Status    enum.CommonStatus `json:"status"    xorm:"null  int default 1 comment('状态; 1-启用，2-停用')"` //状态; 1-启用，2-停用
	Typ       int32             `json:"-"         xorm:"null int default 0 comment('0-系统参数不可更改，不可查询')"`
	CreatedAt enum.JsonTime     `json:"createdAt" xorm:"created"`
	UpdatedAt enum.JsonTime     `json:"updatedAt" xorm:"updated"`
}

func (s *SysParam) IntValue() int32 {
	if s.Value == "" {
		return 0
	}
	atoi, _ := strconv.Atoi(s.Value)
	return int32(atoi)
}
func (s *SysParam) IntJson(out interface{}) {
	if s.Value == "" {
		return
	}
	_ = json.Unmarshal([]byte(s.Value), out)
}

type SysOperateLog struct {
	Id             int64         `json:"id"              xorm:"not null pk autoincr BIGINT(20)"`
	UserId         int64         `json:"userId"        xorm:"null BIGINT(20) default 0 comment('用户ID')"` //上级ID
	UserName       string        `json:"userName"        xorm:"not null VARCHAR(60) comment('用户昵称')"`    //登陆用户名
	Path           string        `json:"path"            xorm:"null VARCHAR(120) comment('请求的路径')"`
	Opt            string        `json:"opt"             xorm:"null VARCHAR(255) comment('操作内容')"`       //操作内容
	Ip             string        `json:"ip"              xorm:"null VARCHAR(255) comment('Ip地址')"`       //操作内容
	TraceId        string        `json:"traceId"         xorm:"null VARCHAR(255) comment('TraceId')"`    //TraceId
	Cost           int64         `json:"cost"            xorm:"null BIGINT(20) default 0 comment('耗时')"` // 耗时
	RequestBody    string        `json:"requestBody"     xorm:"null text comment('请求参数')"`
	ResponseBody   string        `json:"responseBody"    xorm:"null text comment('响应参数')"`
	ResponseStatus int           `json:"responseStatus"  xorm:"null int default 0 comment('响应状态')"`
	BizStatus      int           `json:"bizStatus"       xorm:"null int default 0 comment('业务响应状态')"`
	Message        string        `json:"message"         xorm:"null text comment('消息')"`
	CreatedAt      enum.JsonTime `json:"createdAt"       xorm:"created"`
}
type Rs struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	TraceId string `json:"traceId"`
}

type SysClassify struct {
	Id        int64             `json:"id"              xorm:"NOT NULL PK AUTOINCR BIGINT(20)"`
	ParentId  int64             `json:"parentId"        xorm:"null BIGINT(20) default 0 comment('上级ID')"`          //上级ID
	Name      string            `json:"name"            xorm:"NOT NULL VARCHAR(64)  COMMENT('参数名称')"`              //字典唯一的KEY
	Code      string            `json:"code"            xorm:"NOT NULL VARCHAR(256) unique(code) COMMENT('code')"` //编码
	Extra     string            `json:"extra"           xorm:"NOT NULL VARCHAR(256) COMMENT('参数值')"`               // 额外的参数值
	Status    enum.CommonStatus `json:"status"          xorm:"NOT NULL int default 1 comment('状态; 1-启用，2-停用')"`    //
	IsDel     int32             `json:"-"               xorm:"null int default 0 comment('状态; 0：正常，1-删除')"`
	CreatedAt enum.JsonTime     `json:"createdAt"       xorm:"created"`
	UpdatedAt enum.JsonTime     `json:"updatedAt"       xorm:"updated"`
}
