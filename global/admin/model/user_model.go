package sysModel

import (
	"crypto/md5"
	"encoding/hex"
	"gitee.com/jokces/kit/enum"
	"strings"
)

type RoleType int32 //角色数据权限
const (
	ALL      RoleType = iota + 1 //all,所有权限
	CHILDREN          = 2        //自己与下级的权限
	SELF              = 3        //自己本身的权限
)

const (
	LoginExpired = 24 * 3600 //s
	CodeExpired  = 300       //s
)

type SysUser struct {
	Id           int64             `json:"id"             xorm:"not null pk autoincr BIGINT(20)"`
	Uid          string            `json:"uid"            xorm:"not null VARCHAR(32) unique(uid) comment('分布式的UID')"`
	RoleId       int64             `json:"roleId"         xorm:"null BIGINT(20) default 0 comment('角色ID')"`              //角色ID
	DepId        int64             `json:"depId"          xorm:"null BIGINT(20) default 0 comment('部门ID')"`              //部门ID
	UserName     string            `json:"userName"       xorm:"not null VARCHAR(60) unique(user_name) comment('登录账号')"` //登录账号
	NickName     string            `json:"nickName"       xorm:"null VARCHAR(64) comment('昵称')"`                         //昵称
	Phone        string            `json:"phone"          xorm:"null VARCHAR(20) unique(phone) comment('手机号')"`          //手机号
	Email        string            `json:"email"          xorm:"null VARCHAR(64) unique(email) comment('邮箱')"`           //邮箱
	Pass         string            `json:"-"              xorm:"null VARCHAR(32)  comment('密码')"`
	Status       enum.CommonStatus `json:"status"         xorm:"null  int default 1 comment('状态; 1-启用，2-停用')"` //状态; 1-启用，2-停用
	Salt         string            `json:"-"              xorm:"null VARCHAR(6) comment('盐')"`
	LoginTime    int64             `json:"loginTime"      xorm:"null  BIGINT(20) default 0 comment('登录时间')"` //登录时间
	UpdatedAt    enum.JsonTime     `json:"updatedAt"      xorm:"updated"`
	CreatedAt    enum.JsonTime     `json:"createdAt"      xorm:"created"`
	LikeUserName string            `json:"-" xorm:"-"`
}

func (su SysUser) CheckOneParam() bool {
	return su.Id < 0 && su.Email == "" && su.Phone == "" && su.Uid == "" && su.UserName == ""
}

func (su SysUser) CheckPass(pass string) bool {
	//密码的算法控制，md5（密码+盐）
	buffer := pass + su.Salt
	h := md5.New()
	h.Write([]byte(buffer))
	password := hex.EncodeToString(h.Sum(nil))
	if strings.ToLower(su.Pass) == strings.ToLower(password) {
		return true
	}
	return false
}

// AdminUserToken 后台管理员相关的对象处理
type AdminUserToken struct {
	Id       int64    `json:"id"              form:"id"`       //用户ID
	Uid      string   `json:"uid"             form:"uid"`      //登陆用户的UId
	UserName string   `json:"userName"        form:"userName"` //登陆名称
	RoleId   int64    `json:"roleId"          form:"roleId"`   //角色ID
	DepId    int64    `json:"depId"           form:"depId"`    //部门ID
	DepCode  string   `json:"depCode"         form:"depCode"`  //部门编码
	RoleTyp  RoleType `json:"roleTyp"         form:"roleTyp"`  //角色数据权限类型 1-所有权限,2-下级权限,3-本部门权限
	RootTyp  int32    `json:"rootTyp"         form:"rootTyp"`  //角色类型 1-超级
	GroupId  string   `json:"groupId"         form:"groupId"`  //角色分组的ID
	RoleName string   `json:"-"`                               //角色名称
	DepName  string   `json:"-"`                               //部门名称
	NickName string   `json:"-"`                               //昵称
	Phone    string   `json:"-"`                               //手机号
	Email    string   `json:"-"`                               //邮箱
}

func (aut *AdminUserToken) RootRole() bool {
	return aut.RootTyp == 1
}
func (aut *AdminUserToken) DepGet() *SysDep {
	dep := &SysDep{}
	if aut.Children() {
		dep.LikeDepCode = aut.DepCode
	} else if aut.Self() {
		dep.Id = aut.DepId
	} else if aut.All() {
		dep.Id = 0
	} else {
		dep.Id = 9223372036854775807
	}
	return dep
}
func (aut *AdminUserToken) DepIdGet() int64 {
	if aut.All() {
		return 0
	}
	if aut.Self() {
		return aut.DepId
	}
	if aut.Children() {
		return aut.DepId
	}
	//返回最大值进处理
	return 9223372036854775807
}
func (aut *AdminUserToken) All() bool {
	return aut.RoleTyp == ALL
}
func (aut *AdminUserToken) Children() bool {
	return aut.RoleTyp == CHILDREN
}
func (aut *AdminUserToken) Self() bool {
	return aut.RoleTyp == SELF
}
