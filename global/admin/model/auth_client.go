package sysModel

import (
	"gitee.com/jokces/kit/enum"
)

type GrantType string

func (g GrantType) ToString() string {
	return string(g)
}

type RespType string

func (r RespType) ToString() string {
	return string(r)
}

type ScopeType string

func (r ScopeType) ToString() string {
	return string(r)
}

const (
	Password          GrantType = "password"           //密码授权方式
	RefreshToken      GrantType = "refresh_token"      //刷新token
	AuthorizationCode GrantType = "authorization_code" //授权码
	ClientCredentials GrantType = "client_credentials" //客户端
	Mobile            GrantType = "mobile"             //手机号
	Email             GrantType = "email"              //邮箱
	Wechat            GrantType = "wechat"             //微信
	RespCode          RespType  = "code"               //返回验证码;
	ScopeServer       RespType  = "server"             //服务端资源权限;
)

// SysOauthClient 授权客户端的信息
type SysOauthClient struct {
	Id                   int64          `json:"id"                         xorm:"not null pk autoincr BIGINT(20)"`
	ClientName           string         `json:"clientName"                 xorm:"not null VARCHAR(256) comment('客户端名称')"`                                                                           //客户端名称
	ClientId             string         `json:"clientId"                   xorm:"not null VARCHAR(64) unique(client_id) comment('客户端ID')"`                                                          //客户端ID
	ClientSecret         string         `json:"clientSecret"               xorm:"not null VARCHAR(256) comment('客户端秘钥')"`                                                                           //客户端秘钥
	ResourceIds          string         `json:"resourceIds"                xorm:"not null VARCHAR(256) comment('资源集合ID')"`                                                                          //资源集合ID
	Authorities          string         `json:"authorities"                xorm:"not null VARCHAR(256) comment('权限集合')"`                                                                            //权限集合
	Scope                ScopeType      `json:"scope"                      xorm:"null VARCHAR(64) comment('授权范围')"`                                                                                 //授权范围
	GrantTypes           GrantType      `json:"grantTypes"                 xorm:"not null VARCHAR(256) comment('授权类型,多个用,分割:password,refresh_token,authorization_code,client_credentials,mobile')"` //授权类型:分割:password,refresh_token,authorization_code,client_credentials,mobile
	RedirectUri          string         `json:"redirectUri"                xorm:"null VARCHAR(256) comment('回调地址')"`                                                                                //回调地址
	AccessTokenValidity  int32          `json:"accessTokenValidity"        xorm:"not null int default 0 comment('访问令牌有效期（秒）')"`                                                                     //访问令牌有效期（秒）
	RefreshTokenValidity int32          `json:"refreshTokenValidity"       xorm:"not null int default 0 comment('刷新令牌有效期（秒）')"`                                                                     //刷新令牌有效期（秒）
	AdditionalInfo       string         `json:"additionalInfo"             xorm:"null text comment('附加信息JSON字符串')"`                                                                                 //附加信息JSON字符串
	UpdatedAt            enum.JsonTime  `json:"updatedAt"                  xorm:"updated"`
	CreatedAt            enum.JsonTime  `json:"createdAt"                  xorm:"created"`
	IsDel                enum.DelStatus `json:"-"                          xorm:"int default 0 comment('状态; 0：正常，1-删除')"` //状态; 0：正常，1-删除
}

type AdditionalInfo struct {
	CaptchaFlag int `json:"captchaFlag"` //是否需要验证码;1:表示需要，其他表示不需要
}

func (c *SysOauthClient) QueryOneVal() bool {
	return c.Id < 0 && c.ClientId == ""
}

// NeedCheckCode 验证是否需要验证码的操作
// { "enc_flag":"0","captcha_flag":"0"}
func (c *SysOauthClient) NeedCheckCode() bool {
	rs := &AdditionalInfo{}
	if c.AdditionalInfo == "" {
		//默认不需要
		return false
	}
	if err := enum.ToObject(c.AdditionalInfo, rs); err != nil {
		return false
	}
	return rs.CaptchaFlag == 1
}

// DoesRootClient 判断是否为根客户端请求
func (c *SysOauthClient) DoesRootClient() bool {
	return c.Id == 1
}
