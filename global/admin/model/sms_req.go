package sysModel

type MsgSendReq struct {
	MsgRandReq
	Email string `json:"email" form:"email" binding:"required"` //邮箱地址
}
type MsgCheckReq struct {
	MsgRandReq
	Email string `json:"email" form:"email" binding:"required"` //邮箱地址
	Code  string `json:"code" form:"code" binding:"required"`   //验证码
}

type MsgRandReq struct {
	RandCode string `json:"randCode" form:"randCode" binding:"required"` //发送邮件的随机口令码
}

type MsgSmsReq struct {
	MsgRandReq
	Mobile string `json:"mobile" form:"mobile" binding:"required,min=6,max=20"` //国际手机号,不输入国际区号，默认为86
}
type MsgSmsCheckReq struct {
	MsgRandReq
	Mobile string `json:"mobile" form:"mobile" binding:"required,min=6,max=20"` //国际手机号,不输入国际区号，默认为86
	Code   string `json:"code" form:"code" binding:"required"`                  //验证码
}

type CheckCodeResp struct {
	Success bool `json:"success"` //成功是true
}
