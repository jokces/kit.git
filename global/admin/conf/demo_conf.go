package conf

import (
	"gitee.com/jokces/kit/db/mysql"
	"gitee.com/jokces/kit/db/redis"
	"gitee.com/jokces/kit/global/glconf"
)

func InitConfig() *glconf.Config {
	c := &glconf.Config{
		Env:  "release",
		Lang: "CH",
	}

	c.Mysql = glconf.XormConf{
		Config: mysql.Config{
			ShowSQL: true,
			Master: mysql.Conn{
				Username: "root",
				Password: "OnlyOne2023*",
				HostPort: "124.222.135.192:3306",
				Database: "kit_demo",
			},
		},
		Enable: true,
	}

	c.Redis = glconf.RedisConf{
		Config: redis.Config{
			Addr: "124.222.135.192:6379",
			DB:   10,
			Options: redis.Options{
				Password: "OnlyOne2023*",
			},
		},
		Enable: true,
	}
	c.Oss = glconf.FileConf{
		LocalConf: glconf.LocalConf{
			FilePath:     "/image",
			DownloadPath: "/image/admin",
			FileUrl:      "http://127.0.0.1",
			Enable:       true,
		},
	}
	tp := make(map[string]*glconf.TemplateConf)
	tp["CODE"] = &glconf.TemplateConf{
		Subject: "JS",
		Txt:     "您的验证码是：%s,5分钟内有效！",
	}
	c.Msg = glconf.MsgConf{
		EmailConf: glconf.EmailConf{
			User:   "liangyao10086@163.com",
			Pass:   "sss",
			Host:   "smtp.163.com",
			Enable: true,
		},
		AliSmsConf: glconf.AliSmsConf{
			RegionId:        "cn-hangzhou",
			AccessKey:       "xxxxx",
			AccessKeySecret: "xxxxx",
			SignName:        "xxxxx",
			TempChId:        "xxxxx",
			TempEnId:        "xxxxx",
			Enable:          true,
		},
		MsgTemplate: glconf.MsgTemplate{
			Temp:   tp,
			MaxDay: 5,
			IsMock: true,
		},
	}

	return c
}
