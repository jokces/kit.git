package transet

import "gitee.com/jokces/kit/errc"

var (
	UserNotFund  = errc.MutiMsg{CN: "用户未注册", EN: "user not join"}
	UserDisabled = errc.MutiMsg{CN: "用户已禁用", EN: "user unable"}
	UserNotMatch = errc.MutiMsg{CN: "密码不正确", EN: "password not match"}
)
