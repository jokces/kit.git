package test

import (
	"context"
	"fmt"
	sysModel "gitee.com/jokces/kit/global/admin/model"
	"gitee.com/jokces/kit/tests"
	"testing"
)

func Test_SMS_Send(t *testing.T) {
	uri := Api + "/admin/msg/send"

	req := sysModel.MsgSmsReq{
		MsgRandReq: sysModel.MsgRandReq{
			RandCode: "1111111",
		},
		Mobile: "13368466998",
	}
	out := make(map[string]interface{})
	if err := tests.Post(uri, nil, req, &out); err != nil {
		panic(err)
		return
	}
	tests.PrintBeautifyJSON(out)
}

func Test_SMS_Email(t *testing.T) {
	uri := Api + "/admin/email/send"

	req := sysModel.MsgSendReq{
		MsgRandReq: sysModel.MsgRandReq{
			RandCode: "22222",
		},
		Email: "814200246@qq.com",
	}
	out := make(map[string]interface{})
	if err := tests.Post(uri, nil, req, &out); err != nil {
		panic(err)
		return
	}
	tests.PrintBeautifyJSON(out)
}

func Test_SMS_Handler(t *testing.T) {
	id := 1
	if err := DemoTodo()(context.Background(), id, func(ctx context.Context, fid int) error {
		fmt.Printf("具体的业务逻辑：id=%v,fid=%v\n", id, fid)
		return nil
	}); err != nil {
		panic(err)
	}
}

type DemoHandler func(ctx context.Context, id int, fun func(ctx context.Context, id int) error) error

func DemoTodo() DemoHandler {
	return func(ctx context.Context, id int, fun func(ctx context.Context, fid int) error) error {
		fmt.Printf("切面所执行的数据：id:%v\n", id)
		return fun(ctx, id+1)
	}
}
