package test

import (
	"fmt"
	sysModel "gitee.com/jokces/kit/global/admin/model"
	"gitee.com/jokces/kit/global/admin/token"
	"gitee.com/jokces/kit/global/admin/util"
	"gitee.com/jokces/kit/tests"
	"testing"
	"time"
)

var Api = "http://127.0.0.1"
var AccessToken = "AFeYYR-ajtuDwEI9BbkgPUgWrH9t350Ew8aWr2o6reLuv_-kw3Z0eFKHO686Qj9813uILg1jAASnnfwU3n5MYK1fLfG1"
var ReToken = "P_j1II7msy6v1JUImxZKoBsNLQe_quS7PMvzY5ozX4Qyh6sPbNieYdhL_7bbXjdg74Uh2O0IQDNB7OKsjBrm931uGOIz"

func Test_Token(t *testing.T) {
	ts := time.Now().Unix() + 1800
	for i := 0; i < 1000; i++ {
		inToken := &token.Token{
			Id:  1,
			Exp: ts,
			Typ: 1,
		}
		generateToken, err := token.GenerateToken(inToken)
		if err != nil {
			panic(err)
			return
		}

		//generateToken := "ap3qnJO9SB1Sf8A0WaBUOzQxvIdMbRrj6lZVTkFurn7/A5xJYHs+QM1AD4+hkA2RRO0CbKM1VGbx2txYPiiXgYDeSVib"

		parseToken, b, err := token.ParseToken(generateToken)
		if err != nil {
			panic(err)
			return
		}
		fmt.Printf("generateToken=%v b=%v parseToken=%v ts=%v\n", generateToken, b, parseToken, 1)
	}
}

func Test_Plat_Pass_Login(t *testing.T) {
	uri := Api + "/admin/oauth/token"

	req := sysModel.AccessTokenReq{
		ClientId:  "000000",
		GrantType: "password",
		UserName:  "admin",
		Password:  util.Md5("123456"),
	}
	out := make(map[string]interface{})
	if err := tests.Post(uri, nil, req, &out); err != nil {
		panic(err)
		return
	}
	tests.PrintBeautifyJSON(out)
}

func Test_Plat_ReToken(t *testing.T) {
	uri := Api + "/admin/oauth/token?refreshToken=" + ReToken
	out := make(map[string]interface{})
	if err := tests.Get(uri, nil, &out); err != nil {
		panic(err)
		return
	}
	tests.PrintBeautifyJSON(out)
}

func Test_Plat_Captcha(t *testing.T) {
	uri := Api + "/admin/oauth/captcha"
	out := make(map[string]interface{})
	if err := tests.Get(uri, nil, &out); err != nil {
		panic(err)
		return
	}
	tests.PrintBeautifyJSON(out)
}

func Test_Plat_LoginOut(t *testing.T) {
	uri := Api + "/admin/oauth/out"
	out := make(map[string]interface{})
	headers := map[string]string{}
	headers[sysModel.TokenHeader] = AccessToken
	if err := tests.Post(uri, headers, nil, &out); err != nil {
		panic(err)
		return
	}
	tests.PrintBeautifyJSON(out)
}
