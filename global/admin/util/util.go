package util

import (
	"crypto/md5"
	"encoding/hex"
	"strconv"
	"strings"
)

// Md5 返回一个32微的md5数据
func Md5(buffer string) string {
	h := md5.New()
	h.Write([]byte(buffer))
	password := hex.EncodeToString(h.Sum(nil))
	return strings.ToLower(password)
}

// Md516 返回一个16位md5加密后的字符串
func Md516(data string) string {
	return Md5(data)[8:24]
}

func AdminLoginPass(userName, salt string) string {
	//密码的算法控制，md5（用户名称+盐）
	buffer := userName + salt
	return Md5(buffer)
}

func AdminLoginToken(userName string, loginTime int64) string {
	//密码的算法控制，md5（用户名称+登录时间）
	buffer := userName + strconv.FormatInt(loginTime, 10)
	return Md5(buffer)
}
