package sysApi

import (
	"gitee.com/jokces/kit/ginutil"
	sysModel "gitee.com/jokces/kit/global/admin/model"
	"github.com/gin-gonic/gin"
)

// @Summary [类别]查询类别
// @Description 类别列表
// @Tags [admin]-类别管理
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.ClassifyListReq true "request param"
// @Success 200 {object} sysModel.SysClassify "success"
// @Router /admin/v1/classify/list [get]
func (r *AdminRouter) classifyList(ctx *gin.Context) {
	in := &sysModel.ClassifyListReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if rs, err := r.s.ClassifyList(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}

// @Summary [类别]添加
// @Description 添加类别
// @Tags [admin]-类别管理
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.ClassifyAddReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/classify/add [post]
func (r *AdminRouter) classifyAdd(ctx *gin.Context) {
	in := &sysModel.ClassifyAddReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.AddClassify(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [类别]编辑
// @Description 编辑类别
// @Tags [admin]-类别管理
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.ClassifyEditReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/classify/edit [post]
func (r *AdminRouter) classifyEdit(ctx *gin.Context) {
	in := &sysModel.ClassifyEditReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.EditClassify(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [类别]删除
// @Description 删除类别
// @Tags [admin]-类别管理
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.CommonIdReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/classify/del [post]
func (r *AdminRouter) classifyDel(ctx *gin.Context) {
	in := &sysModel.CommonIdReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.DelClassify(ctx, in.Id); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}
