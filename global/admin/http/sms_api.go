package sysApi

import (
	"gitee.com/jokces/kit/enum"
	"gitee.com/jokces/kit/errc"
	"gitee.com/jokces/kit/ginutil"
	sysModel "gitee.com/jokces/kit/global/admin/model"
	"gitee.com/jokces/kit/typ"
	"github.com/gin-gonic/gin"
)

// @Summary 发送验证码
// @Description 发送短信,未登录状态
// @Tags 短信或邮件
// @Param Request body sysModel.MsgSmsReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /:appType/msg/send [post]
func (r *AdminRouter) smsSend(ctx *gin.Context) {
	in := &sysModel.MsgSmsReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.SendSmsCode(in.Mobile, in.RandCode); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary 短信验证码是否正确
// @Description 校验验证码
// @Tags 短信或邮件
// @Param Request body sysModel.MsgSmsCheckReq true "request param"
// @Success 200 {object} sysModel.CheckCodeResp "success"
// @Router /:appType/msg/check [post]
func (r *AdminRouter) smsCheck(ctx *gin.Context) {
	in := &sysModel.MsgSmsCheckReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if ok, err := r.s.VerifySmsCode(typ.GlobalMobile(in.Mobile), in.RandCode, in.Code, false); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, &sysModel.CheckCodeResp{
			Success: ok,
		})
	}
}

// @Summary 发送验证码
// @Description 发送短信,登录状态
// @Tags 短信或邮件
// @Param AX-Access-Token header string true "auth token"
// @Param Request body sysModel.MsgRandReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /:appType/msg/token/send [post]
func (r *AdminRouter) smsSendWithToken(ctx *gin.Context) {
	appType := ctx.Param("appType")
	if ok := sysModel.ValidateType(appType); !ok {
		ginutil.RespErr(ctx, errc.ErrAuthExpired.MultiMsg("只支持：admin or api"))
		return
	}
	in := &sysModel.MsgRandReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}

	var mobile string
	switch appType {
	case sysModel.AdminType:
		exist, token := r.s.GetAdminUser(ctx)
		if !exist {
			ginutil.RespErr(ctx, errc.ErrNeedLogin.MultiMsg("未登录"))
			return
		}
		user, err := r.s.UserOne(ctx, sysModel.SysUser{Id: token.Id, Status: enum.ABLE})
		if err != nil || user == nil {
			ginutil.RespErr(ctx, errc.ErrNeedLogin.MultiMsg("账户不存在或已禁用"))
			return
		}
		mobile = user.Phone
		break
	case sysModel.ApiType:
		exist, session := r.s.GetApiUser(ctx)
		if !exist {
			ginutil.RespErr(ctx, errc.ErrNeedLogin.MultiMsg("未登录"))
			return
		}
		user, err := r.s.AccountOne(ctx, &sysModel.AccountSession{Id: session.Id, UserStatus: enum.ABLE})
		if err != nil || user == nil {
			ginutil.RespErr(ctx, errc.ErrNeedLogin.MultiMsg("账户不存在或已禁用"))
			return
		}
		mobile = user.Mobile
		break
	}
	if mobile == "" {
		ginutil.RespErr(ctx, errc.ErrNeedLogin.MultiMsg("账户手机号码不存在"))
		return
	}
	if err := r.s.SendSmsCode(mobile, in.RandCode); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary 发送邮件
// @Description 发送邮件,未登录状态
// @Tags 短信或邮件
// @Param Request body sysModel.MsgSendReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /:appType/email/send [post]
func (r *AdminRouter) sendEmail(ctx *gin.Context) {
	in := &sysModel.MsgSendReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.SendEmailCode(in.Email, in.RandCode); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary 眼见验证
// @Description 从session中获取邮箱地址,然后发送邮件
// @Tags 短信或邮件
// @Param Request body sysModel.MsgCheckReq true "request param"
// @Success 200 {object} sysModel.CheckCodeResp "success"
// @Router /:appType/email/check [post]
func (r *AdminRouter) checkEmail(ctx *gin.Context) {
	in := &sysModel.MsgCheckReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if ok, err := r.s.VerifyCode(in.Email, in.RandCode, in.Code, false); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, &sysModel.CheckCodeResp{
			Success: ok,
		})
	}
}

// @Summary 发送邮件
// @Description 发送邮件,登录状态
// @Tags 短信或邮件
// @Param AX-Access-Token header string true "auth token"
// @Param Request body sysModel.MsgRandReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /:appType/email/token/send [post]
func (r *AdminRouter) emailSendWithToken(ctx *gin.Context) {
	appType := ctx.Param("appType")
	if ok := sysModel.ValidateType(appType); !ok {
		ginutil.RespErr(ctx, errc.ErrAuthExpired.MultiMsg("只支持：admin or api"))
		return
	}

	in := &sysModel.MsgRandReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}

	var email string
	switch appType {
	case sysModel.AdminType:
		exist, token := r.s.GetAdminUser(ctx)
		if !exist {
			ginutil.RespErr(ctx, errc.ErrNeedLogin.MultiMsg("未登录"))
			return
		}
		user, err := r.s.UserOne(ctx, sysModel.SysUser{Id: token.Id, Status: enum.ABLE})
		if err != nil || user == nil {
			ginutil.RespErr(ctx, errc.ErrNeedLogin.MultiMsg("账户不存在或已禁用"))
			return
		}
		email = user.Email
		break
	case sysModel.ApiType:
		exist, session := r.s.GetApiUser(ctx)
		if !exist {
			ginutil.RespErr(ctx, errc.ErrNeedLogin.MultiMsg("未登录"))
			return
		}
		user, err := r.s.AccountOne(ctx, &sysModel.AccountSession{Id: session.Id, UserStatus: enum.ABLE})
		if err != nil || user == nil {
			ginutil.RespErr(ctx, errc.ErrNeedLogin.MultiMsg("账户不存在或已禁用"))
			return
		}
		email = user.Email
		break
	}

	if email == "" {
		ginutil.RespErr(ctx, errc.ErrNeedLogin.MultiMsg("账户邮箱地址不存在"))
		return
	}
	if err := r.s.SendEmailCode(email, in.RandCode); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}
