package sysApi

import (
	"gitee.com/jokces/kit/errc"
	"gitee.com/jokces/kit/ginutil"
	sysModel "gitee.com/jokces/kit/global/admin/model"
	"github.com/gin-gonic/gin"
)

// @Summary [菜单]菜单列表
// @Description 菜单列表
// @Tags [admin]-【基础】权限菜单
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.MenusListSelectedReq true "request param"
// @Success 200 {object} sysModel.SysMenu "success"
// @Router /admin/v1/menu/treeList [get]
func (r *AdminRouter) menuTreeList(ctx *gin.Context) {
	in := &sysModel.MenusListSelectedReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if rs, err := r.s.MenuTreeList(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}

// @Summary [菜单]新增
// @Description 新增菜单
// @Tags [admin]-【基础】权限菜单
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.MenuAddReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/menu/add [post]
func (r *AdminRouter) menuAdd(ctx *gin.Context) {
	in := &sysModel.MenuAddReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.MenuAdd(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [菜单]修改
// @Description 修改菜单
// @Tags [admin]-【基础】权限菜单
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.MenuEditReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/menu/edit [post]
func (r *AdminRouter) menuEdit(ctx *gin.Context) {
	in := &sysModel.MenuEditReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.MenuEdit(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [菜单]删除
// @Description 菜单删除
// @Tags [admin]-【基础】权限菜单
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.CommonIdReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/menu/del [post]
func (r *AdminRouter) menuDel(ctx *gin.Context) {
	in := &sysModel.CommonIdReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if in.Id <= 0 {
		ginutil.RespErr(ctx, errc.ErrParamInvalid.MultiMsg("need id"))
		return
	}
	if err := r.s.MenuDelete(ctx, in.Id); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [资源]资源列表
// @Description 资源能列表
// @Tags [admin]-【基础】权限菜单
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.ActionListQueryReq true "request param"
// @Success 200 {object} sysModel.SysAction "success"
// @Router /admin/v1/action/list [get]
func (r *AdminRouter) actionList(ctx *gin.Context) {
	in := &sysModel.ActionListQueryReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if rs, err := r.s.ActionPageList(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}

// @Summary [资源]资源添加or修改
// @Description 资源添加|修改
// @Tags [admin]-【基础】权限菜单
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.ActionEditReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/action/edit [post]
func (r *AdminRouter) actionEdit(ctx *gin.Context) {
	in := &sysModel.ActionEditReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.ActionSaveOrUpdate(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [资源]资源删除
// @Description 资源删除
// @Tags [admin]-【基础】权限菜单
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.CommonIdReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/action/del [post]
func (r *AdminRouter) actionDel(ctx *gin.Context) {
	in := &sysModel.CommonIdReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.ActionDel(ctx, in.Id); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [资源]资源导入
// @Description 资源导入添加,通过DOC的方式进行添加
// @Tags [admin]-【基础】权限菜单
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.ActionAddDocReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/action/doc [post]
func (r *AdminRouter) actionAddDoc(ctx *gin.Context) {
	in := &sysModel.ActionAddDocReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.ActionAddDoc(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [菜单]菜单资源接口
// @Description 菜单资源接口
// @Tags [admin]-【基础】权限菜单
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.CommonIdReq true "request param"
// @Success 200 {object} sysModel.SysAction "success"
// @Router /admin/v1/bind/menuAction  [get]
func (r *AdminRouter) menuActions(ctx *gin.Context) {
	in := &sysModel.CommonIdReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if rs, err := r.s.MenuBindActionAll(ctx, in.Id); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}

// @Summary [菜单]绑定资源
// @Description 菜单绑定资源
// @Tags [admin]-【基础】权限菜单
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.BindMenuWithActionReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/bind/menuAction [post]
func (r *AdminRouter) bindActions(ctx *gin.Context) {
	in := &sysModel.BindMenuWithActionReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.MenuBindAction(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [角色]角色列表
// @Description 角色列表
// @Tags [admin]-【基础】权限菜单
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.RoleListPageReq true "request param"
// @Success 200 {object} sysModel.SysRole "success"
// @Router /admin/v1/role/list [get]
func (r *AdminRouter) roleList(ctx *gin.Context) {
	in := &sysModel.RoleListPageReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if rs, err := r.s.RolePageList(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}

// @Summary [角色]新增
// @Description 新增角色
// @Tags [admin]-【基础】权限菜单
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.RoleAddReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/role/add [post]
func (r *AdminRouter) roleAdd(ctx *gin.Context) {
	in := &sysModel.RoleAddReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.RoleAdd(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [角色]修改
// @Description 新增角色
// @Tags [admin]-【基础】权限菜单
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.RoleEditReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/role/edit [post]
func (r *AdminRouter) roleEdit(ctx *gin.Context) {
	in := &sysModel.RoleEditReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.RoleEdit(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [角色]删除角色
// @Description 删除角色
// @Tags [admin]-【基础】权限菜单
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.CommonIdReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/role/del [post]
func (r *AdminRouter) roleDel(ctx *gin.Context) {
	in := &sysModel.CommonIdReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.RoleDel(ctx, in.Id); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [角色]权限资源
// @Description 查询角色权限对应的资源
// @Tags [admin]-【基础】权限菜单
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.CommonIdReq true "request param"
// @Success 200 {object} sysModel.RoleActionsResp "success"
// @Router /admin/v1/role/permission [get]
func (r *AdminRouter) rolePermission(ctx *gin.Context) {
	in := &sysModel.CommonIdReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if rs, err := r.s.RolePermissionAll(ctx, in.Id); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}

// @Summary [角色]绑定资源
// @Description 角色绑定资源
// @Tags [admin]-【基础】权限菜单
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.RoleBindActionReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/role/bindActions [post]
func (r *AdminRouter) roleBindAction(ctx *gin.Context) {
	in := &sysModel.RoleBindActionReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.RoleBindAction(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [角色]数据权限
// @Description 角色数据权限
// @Tags [admin]-【基础】权限菜单
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Success 200 {object} sysModel.RoleDat "success"
// @Router /admin/v1/role/dataList [get]
func (r *AdminRouter) roleDataList(ctx *gin.Context) {
	exist, sess := r.s.GetAdminUser(ctx)
	if !exist {
		ginutil.RespErr(ctx, errc.ErrAuthExpired)
		return
	}
	list := r.s.RoleDataList(sess)
	ginutil.RespData(ctx, list)
}
