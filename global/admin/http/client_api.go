package sysApi

import (
	"gitee.com/jokces/kit/ginutil"
	sysModel "gitee.com/jokces/kit/global/admin/model"
	"gitee.com/jokces/kit/typ"
	"github.com/gin-gonic/gin"
)

// @Summary [客户端]查询列表
// @Description 客户端列表
// @Tags [admin]-客户端管理
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.OauthQueryReq true "request param"
// @Success 200 {object} sysModel.SysOauthClient "success"
// @Router /admin/v1/client/list [get]
func (r *AdminRouter) clientList(ctx *gin.Context) {
	in := &sysModel.OauthQueryReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if rs, err := r.s.QueryClientList(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}

// @Summary [客户端]添加
// @Description 添加客户端
// @Tags [admin]-客户端管理
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.OauthClientAddReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/client/add [post]
func (r *AdminRouter) clientAdd(ctx *gin.Context) {
	in := &sysModel.OauthClientAddReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.AddClient(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [客户端]编辑
// @Description 编辑客户端
// @Tags [admin]-客户端管理
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.OauthClientUpdateReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/client/edit [post]
func (r *AdminRouter) clientEdit(ctx *gin.Context) {
	in := &sysModel.OauthClientUpdateReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.UpdateClient(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [客户端]删除
// @Description 删除客户端
// @Tags [admin]-客户端管理
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.CommonIdReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/client/del [post]
func (r *AdminRouter) clientDel(ctx *gin.Context) {
	in := &sysModel.CommonIdReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.DelClient(ctx, in.Id); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [客户端]添加资源组
// @Description 添加资源组
// @Tags [admin]-客户端管理
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.ClientAddResourceReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/client/resource [post]
func (r *AdminRouter) clientResource(ctx *gin.Context) {
	in := &sysModel.ClientAddResourceReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.ClientResource(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [toke]会员token
// @Description 会员token
// @Tags [admin]-token管理
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body typ.PageReq true "request param"
// @Success 200 {object} sysModel.RedisTokenResp "success"
// @Router /admin/v1/token/account [get]
func (r *AdminRouter) tokenAccountList(ctx *gin.Context) {
	in := &typ.PageReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if rs, err := r.s.ApiTokenList(in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}

// @Summary [toke]管理员token
// @Description 管理员token
// @Tags [admin]-token管理
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body typ.PageReq true "request param"
// @Success 200 {object} sysModel.RedisTokenResp "success"
// @Router /admin/v1/token/user [get]
func (r *AdminRouter) tokenUserList(ctx *gin.Context) {
	in := &typ.PageReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if rs, err := r.s.AdminTokenList(in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}

// @Summary [toke]剔除用户
// @Description 剔除用户
// @Tags [admin]-token管理
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.ReTokenReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/token/user [get]
func (r *AdminRouter) userLoginOut(ctx *gin.Context) {
	in := &sysModel.ReTokenReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.LoginOut(in.RefreshToken); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}
