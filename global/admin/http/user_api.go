package sysApi

import (
	"gitee.com/jokces/kit/errc"
	"gitee.com/jokces/kit/ginutil"
	sysModel "gitee.com/jokces/kit/global/admin/model"
	"github.com/gin-gonic/gin"
)

// @Summary [用户管理]查询列表
// @Description 用户列表
// @Tags [admin]-【基础】用户管理
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.UserListReq true "request param"
// @Success 200 {object} sysModel.SysUser "success"
// @Router /admin/v1/user/list [get]
func (r *AdminRouter) userList(ctx *gin.Context) {
	in := &sysModel.UserListReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	exist, sess := r.s.GetAdminUser(ctx)
	if !exist {
		ginutil.RespErr(ctx, errc.ErrNeedLogin)
		return
	}
	if rs, err := r.s.UserPageList(ctx, in, sess); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}

// @Summary [用户管理]添加
// @Description 新增用户
// @Tags [admin]-【基础】用户管理
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.UserAddReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/user/add [post]
func (r *AdminRouter) userAdd(ctx *gin.Context) {
	in := &sysModel.UserAddReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.UserAdd(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [用户管理]启用停用
// @Description 启用停用用户
// @Tags [admin]-【基础】用户管理
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.CommonDisabledReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/user/disabled [post]
func (r *AdminRouter) disabledUser(ctx *gin.Context) {
	in := &sysModel.CommonDisabledReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	exist, sess := r.s.GetAdminUser(ctx)
	if !exist {
		ginutil.RespErr(ctx, errc.ErrNeedLogin)
		return
	}
	if err := r.s.UserDisabled(ctx, in, sess); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [用户管理]编辑
// @Description 编辑用户
// @Tags [admin]-【基础】用户管理
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.UserEditReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/user/edit [post]
func (r *AdminRouter) userEdit(ctx *gin.Context) {
	in := &sysModel.UserEditReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.UserEdit(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [用户管理]修改我的密码
// @Description 重置密码
// @Tags [admin]-【基础】用户管理
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.UserRestMyPassReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/user/myPass [post]
func (r *AdminRouter) userRestMyPass(ctx *gin.Context) {
	in := &sysModel.UserRestMyPassReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	exist, sess := r.s.GetAdminUser(ctx)
	if !exist {
		ginutil.RespErr(ctx, errc.ErrNeedLogin)
		return
	}
	if err := r.s.UserRestPass(ctx, &sysModel.UserRestPassReq{
		Pass:    in.Pass,
		OldPass: in.OldPass,
	}, sess); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [用户管理]修改我的基本信息
// @Description 重置密码
// @Tags [admin]-【基础】用户管理
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.UserRestInfoReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/user/modify [post]
func (r *AdminRouter) userRestMyInfo(ctx *gin.Context) {
	in := &sysModel.UserRestInfoReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	exist, sess := r.s.GetAdminUser(ctx)
	if !exist {
		ginutil.RespErr(ctx, errc.ErrNeedLogin)
		return
	}
	if err := r.s.UserRestInfo(ctx, in, sess); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [用户管理]重置密码
// @Description 重置密码
// @Tags [admin]-【基础】用户管理
// @Accept  application/json
// @Product application/json
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.UserRestPassReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/user/pass [post]
func (r *AdminRouter) userRestPass(ctx *gin.Context) {
	in := &sysModel.UserRestPassReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	exist, sess := r.s.GetAdminUser(ctx)
	if !exist {
		ginutil.RespErr(ctx, errc.ErrNeedLogin)
		return
	}
	if err := r.s.UserRestPass(ctx, in, sess); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [用户管理]用户详情
// @Description 用户详情
// @Tags [admin]-【基础】用户管理
// @Param AX-Access-Token header string true "管理后台Token"
// @Success 200 {object} sysModel.UserInfoResp "success"
// @Router /admin/v1/user/info [get]
func (r *AdminRouter) userInfo(ctx *gin.Context) {
	exist, sess := r.s.GetAdminUser(ctx)
	if !exist {
		ginutil.RespErr(ctx, errc.ErrNeedLogin)
		return
	}
	if rs, err := r.s.UserInfo(ctx, sess); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}

// @Summary [用户管理]用户的菜单
// @Description 用户的菜单
// @Tags [admin]-【基础】用户管理
// @Param AX-Access-Token header string true "管理后台Token"
// @Success 200 {object} sysModel.UserMenus "success"
// @Router /admin/v1/user/menu [get]
func (r *AdminRouter) userMenu(ctx *gin.Context) {
	exist, sess := r.s.GetAdminUser(ctx)
	if !exist {
		ginutil.RespErr(ctx, errc.ErrNeedLogin)
		return
	}
	if rs, err := r.s.UserMenu(ctx, sess); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}
