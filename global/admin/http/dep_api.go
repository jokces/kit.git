package sysApi

import (
	"gitee.com/jokces/kit/errc"
	"gitee.com/jokces/kit/ginutil"
	sysModel "gitee.com/jokces/kit/global/admin/model"
	"github.com/gin-gonic/gin"
)

// @Summary [部门]查询列表
// @Description 部门列表
// @Tags [admin]-【基础】部门管理
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.DepTreeReq true "request param"
// @Success 200 {object} sysModel.SysDep "success"
// @Router /admin/v1/dep/treeList [get]
func (r *AdminRouter) depTreeList(ctx *gin.Context) {
	in := &sysModel.DepTreeReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	exist, sess := r.s.GetAdminUser(ctx)
	if !exist {
		ginutil.RespErr(ctx, errc.ErrAuthExpired)
		return
	}
	if rs, err := r.s.DepTreeList(ctx, in, sess); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}

// @Summary [部门]添加
// @Description 添加部门
// @Tags [admin]-【基础】部门管理
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.DepAddReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/dep/add [post]
func (r *AdminRouter) depAdd(ctx *gin.Context) {
	in := &sysModel.DepAddReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.DepAdd(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [部门]编辑
// @Description 编辑部门
// @Tags [admin]-【基础】部门管理
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.DepEditReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/dep/edit [post]
func (r *AdminRouter) depEdit(ctx *gin.Context) {
	in := &sysModel.DepEditReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.DepEdit(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [部门]删除
// @Description 删除部门
// @Tags [admin]-【基础】部门管理
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.CommonIdReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/dep/del [post]
func (r *AdminRouter) depDel(ctx *gin.Context) {
	in := &sysModel.CommonIdReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if err := r.s.DepDel(ctx, in.Id); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [系统]参数列表
// @Description 参数列表
// @Tags [admin]-【基础】系统参数
// @Param AX-Access-Token header string true "Token"
// @Param Request body sysModel.ParamListReq true "request param"
// @Success 200 {object} sysModel.SysParam "success"
// @Router /admin/v1/param/list [get]
func (r *AdminRouter) paramList(ctx *gin.Context) {
	in := &sysModel.ParamListReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if rs, err := r.s.ParamList(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}

// ParamEdit
// @Summary [系统]编辑参数
// @Description 编辑参数
// @Tags [admin]-【基础】系统参数
// @Param AX-Access-Token header string true "管理后台Token"
// @Param Request body sysModel.ParamEditReq true "request param"
// @Success 200 {object} ginutil.BaseResp "success"
// @Router /admin/v1/param/edit [post]
func (r *AdminRouter) paramEdit(ctx *gin.Context) {
	in := &sysModel.ParamEditReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	exist, sess := r.s.GetAdminUser(ctx)
	if !exist {
		ginutil.RespErr(ctx, errc.ErrAuthExpired)
		return
	}
	if err := r.s.ParamSaveOrUpdate(ctx, in, sess); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespSuccess(ctx)
	}
}

// @Summary [日志]操作日志
// @Description 操作日志
// @Tags [admin]-【基础】操作日志
// @Param AX-Access-Token header string true "AccessToken"
// @Param Request body sysModel.OptListReq true "request param"
// @Success 200 {object} sysModel.SysOperateLog "success"
// @Router /admin/v1/opt/list [get]
func (r *AdminRouter) optAdminList(ctx *gin.Context) {
	in := &sysModel.OptListReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if rs, err := r.s.OperatePageList(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}

// @Summary [基础]字典列表
// @Description 操作日志;意见(TIPS)|民族字典(NATION)|关系(RELATION)
// @Tags [admin]-【基础】字典列表
// @Param AX-Access-Token header string true "AccessToken"
// @Param Request body sysModel.DicListReq true "request param"
// @Success 200 {object} sysModel.SysDic "success"
// @Router /admin/v1/dict/list [get]
func (r *AdminRouter) dictList(ctx *gin.Context) {
	in := &sysModel.DicListReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if rs, err := r.s.DictAdminList(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}
