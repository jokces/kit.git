package sysApi

import (
	sysService "gitee.com/jokces/kit/global/admin/service"
	"strings"
)

var IgeUrl []string

func init() {
	IgeUrl = make([]string, 0)
	IgeUrl = append(IgeUrl,
		"/admin/v1/user/info",
		"/admin/v1/user/menus",
		"/admin/v1/user/out",
		"/admin/v1/user/myPass",
		"/admin/v1/user/modify",
		"/admin/v1/area/list",
		"/admin/v1/dic/list",
		"/admin/v1/param/list",
		"/admin/v1/file/upload",
	)
}

type AdminRouter struct {
	s *sysService.Service
}

func New() *AdminRouter {
	return &AdminRouter{
		s: sysService.NewService(),
	}
}

// AddIngoUrl 添加过滤的URL
func AddIngoUrl(adminApi string) {
	IgeUrl = append(IgeUrl, adminApi)
}

// UrlOk 验证是否放过这个几个请求
func UrlOk(adminApi string) bool {
	for _, apiUrl := range IgeUrl {
		if strings.Contains(apiUrl, adminApi) {
			return true
		}
	}
	return false
}
