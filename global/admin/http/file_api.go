package sysApi

import (
	"gitee.com/jokces/kit/ginutil"
	"github.com/gin-gonic/gin"
)

// @Summary 单个文件上传
// @Description 单个文件上传
// @Tags 文件管理
// @Produce json
// @Accept multipart/form-data
// @Param AX-Access-Token header string true "管理后台Token"
// @Param file formData file true "file"
// @Success 200 {object} sysModel.FileModelResp "success"
// @Router /:appType/file/upload [POST]
func (r *AdminRouter) uploadFile(c *gin.Context) {
	toUpload, err := r.s.FileToUpload(c)
	if err != nil {
		ginutil.RespErr(c, err)
		return
	}
	ginutil.RespData(c, toUpload)
}
