package sysApi

import (
	"gitee.com/jokces/kit/ginutil"
	sysModel "gitee.com/jokces/kit/global/admin/model"
	"github.com/gin-gonic/gin"
)

// @Summary 区域数据
// @Description 区域数据列表
// @Tags 基础数据
// @Param AX-Access-Token header string true "AccessToken"
// @Param Request body sysModel.AreaListReq true "request param"
// @Success 200 {object} sysModel.AreaListResp "success"
// @Router /:appType/area/list [get]
func (r *AdminRouter) areaList(ctx *gin.Context) {
	in := &sysModel.AreaListReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if rs, err := r.s.DicAreaList(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}

// @Summary 字典列表
// @Description 字典列表：意见(TIPS)|民族字典(NATION)|关系(RELATION)
// @Tags 基础数据
// @Param AX-Access-Token header string true "AccessToken"
// @Param Request body sysModel.DicListReq true "request param"
// @Success 200 {object} sysModel.DicReVo "success"
// @Router /:appType/dic/list [get]
func (r *AdminRouter) dicList(ctx *gin.Context) {
	in := &sysModel.DicListReq{}
	err := ginutil.ShouldBind(ctx, in)
	if err != nil {
		ginutil.RespErr(ctx, err)
		return
	}
	if rs, err := r.s.DicList(ctx, in); err != nil {
		ginutil.RespErr(ctx, err)
	} else {
		ginutil.RespData(ctx, rs)
	}
}
