package sysApi

import (
	"gitee.com/jokces/kit/errc"
	"gitee.com/jokces/kit/ginutil"
	sysModel "gitee.com/jokces/kit/global/admin/model"
	"github.com/gin-gonic/gin"
	"github.com/mojocn/base64Captcha"
	"image/color"
)

// @Summary 获取验证码
// @Description 获取验证码
// @Tags [admin]-【基础】登录
// @Produce json
// @Success 200 {object} sysModel.ImageCodeResp "success"
// @Router /:appType/oauth/captcha [GET]
func (r *AdminRouter) captchaCode(c *gin.Context) {
	codeId, base64, code, err := captMake()
	if err != nil {
		ginutil.RespErr(c, errc.ErrParamInvalid.MultiMsg("get code error"))
		return
	}
	err = r.s.CaptchaRedisCode(code, codeId)
	if err != nil {
		ginutil.RespErr(c, errc.ErrParamInvalid.MultiMsg("get code error"))
		return
	}
	ginutil.RespData(c, sysModel.ImageCodeResp{
		Code:   base64,
		CodeId: codeId,
	})
}

// BASE64 验证码的处理
var captchaConfig = base64Captcha.DriverString{
	Height:          60,
	Width:           200,
	NoiseCount:      0,
	ShowLineOptions: 2 | 4,
	Length:          4,
	Source:          "1234567890qwertyuioplkjhgfdsazxcvbnm",
	BgColor: &color.RGBA{
		R: 3,
		G: 102,
		B: 214,
		A: 125,
	},
	Fonts: []string{"wqy-microhei.ttc"},
}
var codeDriver = captchaConfig.ConvertFonts()

// 生成验证码
func captMake() (id, b64s, code string, err error) {
	captcha := base64Captcha.NewCaptcha(codeDriver, base64Captcha.DefaultMemStore)
	//创建验证码
	codeId, content, code := captcha.Driver.GenerateIdQuestionAnswer()
	item, err := captcha.Driver.DrawCaptcha(content)
	if err != nil {
		return "", "", "", err
	}
	b64s = item.EncodeB64string()
	return codeId, b64s, code, nil
}
