# 聚合的功能服务

- 主要用于单体架构模型
- 如果需要使用微服务架构，则可以采用其他组件的方式进行扩展

- 目前预计集成功能
- 后台管理，短信，文件，数据库，缓存，支付，等一些集成的基础功能（暂时使用一个整体的包，不再细分）

- 初始化的数据库信息文件查询
```sql

```


# 注意事项

```text
1. 日志没有更新，所以所使用的包的版本比较老:

  go get go.uber.org/zap@v1.16.0
  google.golang.org/grpc v1.34.1
  
  这两个包需要替换成老版本，就可以使用了。
```


# 获取配置:

```text
1. 目前打的tag有问题，最新版本为v0.10.0 为非可以用版本，需要采用如下的版本
  
   go get gitee.com/jokces/kit@v0.1.5     -- 工具包
   go get gitee.com/jokces/kit/global     -- 全局包，包含了admin 的集成；代码参见： global
   
   
   
```

# 关于时间的问题 mysql 8.0 没有这个时区
```go

package main

import (
	_ "time/tzdata"  //直接引入这个初始化上海的时间
)

func main() {
	//TODO: code
}


```

# 关于git的操作
```text
git tag -a 'v0.0.1' -m 'v0.0.1' 创建一个本地的tag
git tag 查询所有的本地tag
git push 'https://gitee.com/jokces/kit.git' 'v0.0.1'  提交tag
```

# 关于资源权限的处理
```text
1. 设计一个 system_catagral表； 用来记录所有的下级分类资源信息；或者可以直接用字典来答题
    client_ids 就是对应的 权限资源menu_  分组的信息来 维护就OK了，哈哈
```

