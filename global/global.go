package global

import (
	"fmt"
	"gitee.com/jokces/kit/db/mysql"
	"gitee.com/jokces/kit/db/redis"
	"gitee.com/jokces/kit/errc"
	"gitee.com/jokces/kit/global/file"
	"gitee.com/jokces/kit/global/glconf"
	"gitee.com/jokces/kit/global/sms"
	"gitee.com/jokces/kit/global/thirdpay"
)

var (
	db  *mysql.Xorm    //全局数据库
	rds *redis.Client  //redis的数据库处理
	oss *file.File     //文件上传的配置
	msg *sms.MegHandle //消息的处理
	pay *thirdpay.Pay  //支付
	cfg *glconf.Config //配置文件
)

func BootStrap(gf *glconf.Config) {
	if gf != nil {
		cfg = gf
	}

	//设置全局的异常状态返回
	errc.SetLang(gf.Lang)

	//处理配置信息
	if gf.Mysql.Enable {
		db = mysql.NewXorm(gf.Mysql.Config)
	}

	//redis
	if gf.Redis.Enable {
		if client, err := redis.NewRedisClient(gf.Redis.Config); err != nil {
			panic("【redis】 >>>>> config redis err:" + err.Error())
		} else {
			rds = client
		}
	}

	//文件上传
	oss = file.NewFile(gf.Oss)

	//消息
	msg = sms.NewMegHandle(gf.Msg)

	//支付
	if gf.Pay.Enable {
		pay = thirdpay.NewThirdPay(gf.Pay)
	}

	//用来同步数据结构
	if !gf.Release() && gf.Mysql.Enable {
		if err := GetDb().Sync2(glconf.Models...); err != nil {
			panic("【xorm】 >>>>> config sync2 err:" + err.Error())
		}
	}

}
func GetPay() *thirdpay.Pay {
	return pay
}
func GetMsg() *sms.MegHandle {
	return msg
}
func GetDb() *mysql.Xorm {
	return db
}
func GetRedis() *redis.Client {
	return rds
}
func GetFile() *file.File {
	return oss
}
func GetCfg() *glconf.Config {
	return cfg
}

func Close() {
	if db != nil {
		if err := db.Close(); err != nil {
			fmt.Printf("【close】 >>>> close xorm db error:" + err.Error() + "\n")
		}
	}
}
